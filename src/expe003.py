#!/usr/bin/env python3
from pathlib import Path
from sumfinder import Sums
from linuxmem import read_meminfo_stdout


def main():
    sums = Sums.load(Path('../expe001.5/alambix97-meminfo-sums.json'))
    print(f'starting sums ({sums.get_num_equations()} sums):')
    sums.print()

    variables = read_meminfo_stdout(Path('../bug3897/alambix98/20240806-223916-meminfo.stdout'))
    # variables = read_meminfo_stdout(Path('../bug3897/alambix97/20240805-191606-meminfo.stdout'))
    (valid_sums, invalid_sums) = sums.validate_on_variables(variables)
    print(f'invalid sums ({invalid_sums.get_num_equations()} sums):')
    invalid_sums.print()

    print(f'valid sums ({valid_sums.get_num_equations()} sums):')
    valid_sums.print()


main()
