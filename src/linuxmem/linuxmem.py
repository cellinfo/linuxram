#!/usr/bin/env python3
from typing import Dict, List, Tuple
import re
from pathlib import Path
import subprocess
import time

MeminfoVarId = str
MemUnit = int  # in bytes
KIBIBYTE_TO_BYTE = 1024
MIBIBYTE_TO_BYTE = 1024 * 1024


def parse_meminfo_stdout(meminfo_stdout: str) -> Dict[MeminfoVarId, MemUnit]:
    '''
    parses the result of `cat /proc/meminfo`

    example:
        MemTotal:       196498248 kB
        MemFree:          784240 kB
        MemAvailable:   128385580 kB
        Buffers:           31232 kB
        Cached:         127870960 kB
        SwapCached:      1063208 kB
        ...
    '''
    meminfo_data = {}
    for line in meminfo_stdout.split('\n'):
        # eg 'MemAvailable:   128385580 kB'
        match = re.match(r'^(?P<var_name>[^:]+): +(?P<var_value>[0-9]+) kB', line)
        if match:
            meminfo_data[match['var_name']] = int(match['var_value']) * KIBIBYTE_TO_BYTE
    return meminfo_data


def read_meminfo_stdout(meminfo_stdout_file_path: Path) -> Dict[MeminfoVarId, MemUnit]:
    '''
    parses the result of `cat /proc/meminfo`

    example:
        MemTotal:       196498248 kB
        MemFree:          784240 kB
        MemAvailable:   128385580 kB
        Buffers:           31232 kB
        Cached:         127870960 kB
        SwapCached:      1063208 kB
        ...
    '''
    meminfo_data = {}
    with open(meminfo_stdout_file_path, 'rt', encoding='utf8') as meminfo_stdout_file:
        return parse_meminfo_stdout(meminfo_stdout_file.read())
    return meminfo_data


def meminfo_diff(left_meminfo: Dict[MeminfoVarId, MemUnit], right_meminfo: Dict[MeminfoVarId, MemUnit]) -> Dict[MeminfoVarId, MemUnit]:
    diffs = {}
    for var_id in left_meminfo.keys():
        left_val = left_meminfo[var_id]
        right_val = right_meminfo[var_id]
        diffs[var_id] = right_val - left_val
    return diffs


def find_diff(left_meminfo: Dict[MeminfoVarId, MemUnit], right_meminfo: Dict[MeminfoVarId, MemUnit], requested_diff: MemUnit, tolerance: MemUnit = 0) -> Dict[MeminfoVarId, Tuple[MemUnit, MemUnit]]:
    '''returns the variables which values have changed with the speciied amount'''
    assert len(right_meminfo) == len(right_meminfo)
    diffs = {}
    for var_id in left_meminfo.keys():
        left_val = left_meminfo[var_id]
        right_val = right_meminfo[var_id]
        absdif = abs(left_val - right_val)
        if abs(absdif - requested_diff) < tolerance:
            print(f'{var_id}: {left_val} ~= {right_val}')
            diffs[var_id] = (left_val, right_val)
    return diffs


def meminfo_are_equal(left_meminfo: Dict[MeminfoVarId, MemUnit], right_meminfo: Dict[MeminfoVarId, MemUnit], tolerance: MemUnit):
    assert len(left_meminfo) == len(right_meminfo)
    for var_id in left_meminfo.keys():
        left_val = left_meminfo[var_id]
        right_val = right_meminfo[var_id]
        if abs(left_val - right_val) > tolerance:
            return False
    return True


def get_stabilized_meminfo(tolerance: MemUnit) -> Dict[MeminfoVarId, MemUnit]:
    '''retreives meminfo until its variables are stabilized (converged)'''
    window_size = 5
    meminfo_data_fifo = []
    while True:
        print(len(meminfo_data_fifo))
        completed_process = subprocess.run(['cat', '/proc/meminfo'], check=True, capture_output=True)
        meminfo_data = parse_meminfo_stdout(completed_process.stdout.decode())
        meminfo_data_fifo.append(meminfo_data)
        if len(meminfo_data_fifo) > window_size:
            meminfo_data_fifo.pop(0)  # remove the oldest meminfo_data

        if len(meminfo_data_fifo) >= window_size:
            measures_have_converged = True
            for couple_index in range(window_size - 1):
                if not meminfo_are_equal(meminfo_data_fifo[couple_index], meminfo_data_fifo[couple_index + 1], tolerance):
                    measures_have_converged = False
                    break
            if measures_have_converged:
                return meminfo_data_fifo[-1]
        time.sleep(0.5)


ZoneId = str  # eg 'DMA', 'DMA32', 'Normal', 'Movable', 'Device'
ZoneSize = int  # in pages I guess


class NodeZoneinfo():
    zone_to_page: Dict[ZoneId, ZoneSize]

    def __init__(self):
        self.zone_to_page = {}


class ZoneinfoData():
    nodes: List[NodeZoneinfo]  # a node is a cpu I think

    def __init__(self):
        self.nodes = []

    def set_node_zone_size(self, node_id: int, zone_id: ZoneId, zone_size: ZoneSize):
        while node_id >= (len(self.nodes) - 1):
            self.nodes.append(NodeZoneinfo())
        node_zone_info = self.nodes[node_id]
        node_zone_info.zone_to_page[zone_id] = zone_size

    def __str__(self) -> str:
        as_str = ''
        for node_id in range(len(self.nodes)):
            node_zone_info = self.nodes[node_id]
            as_str += f'{node_id}: {str(node_zone_info.zone_to_page)}'
        return as_str

    def get_num_low_watermark_pages(self) -> ZoneSize:
        num_low_watermark_pages = 0
        for node_zone_info in self.nodes:
            for v in node_zone_info.zone_to_page.values():
                num_low_watermark_pages += v
        return num_low_watermark_pages

    @staticmethod
    def read_grepped_zoneinfo_stdout(grepped_zoneinfo_file_path: Path) -> 'ZoneinfoData':
        '''
        parses the result of  cat /proc/zoneinfo | grep -E '(^Node|low)'

        example:
            Node 0, zone      DMA
                    low      4
            Node 0, zone    DMA32
                    low      529
            Node 0, zone   Normal
                    low      34991
            Node 0, zone  Movable
                    low      0
            Node 0, zone   Device
                    low      0
            Node 1, zone      DMA
                    low      0
            Node 1, zone    DMA32
                    low      0
            Node 1, zone   Normal
                    low      36110
            Node 1, zone  Movable
                    low      0
            Node 1, zone   Device
                    low      0
        '''
        zoneinfo_data = ZoneinfoData()
        node_id: int = None
        zone_id: str = None
        with open(grepped_zoneinfo_file_path, 'rt', encoding='utf8') as zoneinfo_stdout_file:

            for line in zoneinfo_stdout_file.readlines():
                # eg 'Node 0, zone      DMA'
                match = re.match(r'^Node (?P<node_id>[0-9]+), zone +(?P<zone_id>[a-zA-Z0-9_]+)', line)
                if match:
                    node_id = int(match['node_id'])
                    zone_id = match['zone_id']

                # eg '        low      529'
                match = re.match(r'^ *low *(?P<node_zone_size>[0-9]+)', line)

                if match:
                    zoneinfo_data.set_node_zone_size(node_id, zone_id, int(match['node_zone_size']))
        return zoneinfo_data


def estimate_mem_available(meminfo_data: Dict[MeminfoVarId, MemUnit], zoneinfo_data: ZoneinfoData) -> MemUnit:

    # see computation of available memory in linux's si_mem_available (in show_mem.c)
    totalreserve_pages = 447676  # don't know where to get the proper value from
    page_size = 4096  # a guess
    num_free_pages = meminfo_data['MemFree'] / page_size
    print(f'free memory : {num_free_pages * page_size / KIBIBYTE_TO_BYTE} KiB')
    num_available_pages = num_free_pages - totalreserve_pages

    num_low_watermark_pages = zoneinfo_data.get_num_low_watermark_pages()

    num_cache_pages = meminfo_data['Active(file)'] / page_size + meminfo_data['Inactive(file)'] / page_size
    num_cache_pages -= min(num_cache_pages / 2, num_low_watermark_pages)
    print(f'cache memory : {num_cache_pages * page_size / KIBIBYTE_TO_BYTE} KiB')
    num_available_pages += num_cache_pages

    num_reclaimable_pages = meminfo_data['SReclaimable'] / page_size + meminfo_data['KReclaimable'] / page_size
    num_reclaimable_pages -= min(num_reclaimable_pages / 2, num_low_watermark_pages)
    print(f'reclaimable memory : {num_reclaimable_pages * page_size / KIBIBYTE_TO_BYTE} KiB')
    num_available_pages += num_reclaimable_pages

    print(f"meminfo's MemAvailable : {meminfo_data['MemAvailable'] / KIBIBYTE_TO_BYTE} KiB")

    print(f'computed available memory : {num_available_pages * page_size / KIBIBYTE_TO_BYTE} KiB')

    diff = num_available_pages * page_size - meminfo_data['MemAvailable']

    print(f"computed - meminfo = {diff / KIBIBYTE_TO_BYTE} KiB ({diff / page_size} pages)")
    return num_available_pages * page_size


def compute_mem_avail(meminfo_stdout_file_path: Path, grepped_zoneinfo_file_path: Path):
    # code to userstand how meminfo's MemAvailable is computed
    meminfo_data = read_meminfo_stdout(meminfo_stdout_file_path)
    print(meminfo_data)

    zoneinfo_data = ZoneinfoData.read_grepped_zoneinfo_stdout(grepped_zoneinfo_file_path)
    print(zoneinfo_data)

    _ = estimate_mem_available(meminfo_data, zoneinfo_data)

    assert meminfo_data['Active'] == (meminfo_data['Active(file)'] + meminfo_data['Active(anon)'])
    assert meminfo_data['Inactive'] == (meminfo_data['Inactive(file)'] + meminfo_data['Inactive(anon)'])
