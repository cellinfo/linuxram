#!/usr/bin/env python3
# given a list of integer variables and their values, this tool aims at finding which varibales can be obtained by the sum of others
# note : this tool is used to find the sums in the variables output by /proc/meminfo
from typing import Dict, List, Set, Tuple
import abc
from pathlib import Path
import json
import subprocess
import copy

VarName = str
VarValue = int
Sum = Dict[VarName, List[VarName]]


class IntVariable():
    name: str
    value: int

    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __repr__(self) -> str:
        return f'{self.name}: {self.value}'


class Sums():

    variables: Set[IntVariable]
    sums: List[Sum]

    def __init__(self) -> None:
        self.variables = set()
        self.sums = []

    def get_num_variables(self):
        return len(self.variables)

    def get_num_equations(self):
        return len(self.sums)

    def get_variables_as_dict(self):
        return {int_var.name: int_var.value for int_var in self.variables}

    @staticmethod
    def load(sums_file_path: Path) -> 'Sums':
        sums = Sums()
        with open(sums_file_path, 'rt', encoding='utf8') as sums_file:
            root_dict = json.loads(sums_file.read())
        assert root_dict['format'] == 'sums-v001'
        sums.sums = root_dict['sums']
        sums.variables = {IntVariable(var_id, var_value) for var_id, var_value in root_dict['variables'].items()}
        return sums

    def save(self, sums_file_path: Path):
        root_dict = {}
        root_dict['format'] = 'sums-v001'
        root_dict['variables'] = {var.name: var.value for var in self.variables}
        root_dict['sums'] = self.sums
        sums_file_path.write_text(json.dumps(root_dict))

    def print(self):
        equation_index = 0
        for s in self.sums:
            print(f"{equation_index}: {s['total']} = {' + '.join([comp_id for comp_id in s['components']])}")
            equation_index += 1

    @staticmethod
    def sum_is_valid_with_variables(_sum: Sum, variables: Dict[VarName, VarValue]):
        # print(f"checking sum: {_sum['total']} = {' + '.join([comp_id for comp_id in _sum['components']])}")
        # print(f'against {variables}')
        total_var_id = _sum['total']
        components = _sum['components']
        real_total_value = variables[total_var_id]

        computed_total_value = 0
        for component_var_id in components:
            if component_var_id in variables.keys():
                computed_total_value += variables[component_var_id]
            else:
                # we assume that a non existing variable has a value of 0 because variables with values of 0 are often removed for optimization purpose
                pass
        # print(f"total value : computed = {computed_total_value}, real = {real_total_value}")
        return computed_total_value == real_total_value

    def validate_on_variables(self, variables: Dict[VarName, VarValue]) -> Tuple['Sums', 'Sums']:
        """
        returns the tuple valid_sums, invalid_sums after checking each of the sum of input_sum againts the given variables
        """
        valid_sums: Sums = Sums()
        invalid_sums: Sums = Sums()
        for _sum in self.sums:
            if Sums.sum_is_valid_with_variables(_sum, variables):
                valid_sums.sums.append(_sum)
            else:
                invalid_sums.sums.append(_sum)
        return (valid_sums, invalid_sums)

    @staticmethod
    def sums_are_equal(sum1: Sum, sum2: Sum) -> bool:
        if sum1['total'] != sum2['total']:
            return False
        components1 = sum1['components']
        components2 = sum2['components']
        if len(components1) != len(components2):
            return False
        components1_as_str = '+'.join(components1)
        components2_as_str = '+'.join(components2)
        if components1_as_str != components2_as_str:
            # print(f'{components1_as_str} differs from:')
            # print(f'{components2_as_str}')
            return False
        return True

    def add_sum(self, other_sum: Sum):
        sum_alread_exists = False
        for existing_sum in self.sums:
            if Sums.sums_are_equal(existing_sum, other_sum):
                sum_alread_exists = True
                # print(f'sum {other_sum} already exists {existing_sum}')
                break
        if not sum_alread_exists:
            # print('sum doesnt already exist... see if its a valid sum')
            # print(f'add_sum: checking validity of {other_sum}')

            # create the veriables if they don't exist yet
            vars_as_dict = self.get_variables_as_dict()
            for var_name in other_sum['components'] + [other_sum['total']]:
                if var_name not in vars_as_dict.keys():
                    self.variables.add(IntVariable(var_name, 0))

            vars_as_dict = self.get_variables_as_dict()
            if Sums.sum_is_valid_with_variables(other_sum, vars_as_dict):
                # print(f'add_sum: new sum {other_sum}')
                self.sums.append(other_sum)

    def add_sums(self, other_sums: 'Sums'):

        other_sum_index = 0
        for other_sum in other_sums.sums:
            # print(f'add_sums: sum {other_sum_index} : {other_sum}')
            self.add_sum(other_sum)
            other_sum_index += 1

    @staticmethod
    def merge_sums(input_sums: List['Sums']) -> 'Sums':
        for base_sums_index in range(len(input_sums)):
            sums = copy.deepcopy(input_sums[base_sums_index])
            for other_sums_index in range(len(input_sums)):
                if other_sums_index != base_sums_index:
                    other_sums = input_sums[other_sums_index]
                    sums.add_sums(other_sums)
                    valid_sums, invalid_sums = sums.validate_on_variables(other_sums.get_variables_as_dict())
                    sums = valid_sums
            return sums


class ISumHandler(abc.ABC):

    @abc.abstractmethod
    def on_sum_found(self, total: IntVariable, components: List[IntVariable]):
        pass

    @abc.abstractmethod
    def on_end(self):
        pass


class SumPrinter(ISumHandler):

    def __init__(self):
        self.num_sums = 0

    def on_sum_found(self, total: IntVariable, components: List[IntVariable]):
        print(f"{total} = sum({components})")
        self.num_sums += 1

    def on_end(self):
        pass


class SumExporter(ISumHandler):
    '''handler that exports the sums as a graphviz dot file
    '''
    sum_file_path: Path
    sums: Sums

    def __init__(self, sum_file_path: Path):
        self.sum_file_path = sum_file_path
        self.variables = set()
        self.sums = Sums()

    def on_sum_found(self, total: IntVariable, components: List[IntVariable]):
        self.sums.variables.add(total)
        for component in components:
            self.sums.variables.add(component)
        print(f"sum {len(self.sums.sums)} found: {total.name} = {' + '.join([comp.name for comp in components])}")
        self.sums.sums.append({'total': total.name, 'components': [comp.name for comp in components]})

    def on_end(self):
        print(f'exporting sums to {self.sum_file_path}')
        self.sums.save(self.sum_file_path)


def explore(total: IntVariable, components: List[IntVariable], cur_sum: VarValue, cur_var_index: int, contrib_components: List[IntVariable], sum_handler: ISumHandler):
    # for i in range(cur_var_index):
    #     print('  ', end='')
    # print(f"exploring: cur_sum={cur_sum}, contrib_components={contrib_components}, cur_var={components[cur_var_index]}")
    for cur_var_presence in [False, True]:
        new_contrib_components = contrib_components.copy()
        new_sum = cur_sum
        if cur_var_presence:
            new_sum += components[cur_var_index].value
            new_contrib_components.append(components[cur_var_index])
        # print(f"new_sum={new_sum}")
        if new_sum == total.value:
            sum_handler.on_sum_found(total, new_contrib_components)
        elif new_sum < total.value and cur_var_index < len(components) - 1:
            explore(total, components, new_sum, cur_var_index + 1, new_contrib_components, sum_handler)


def find_numbers_summing_to(total: IntVariable, components: List[IntVariable], sum_handler: ISumHandler):
    # print(f'finding which combinations of {components} sum to {total}')
    explore(total, components, cur_sum=0, cur_var_index=0, contrib_components=[], sum_handler=sum_handler)


def optimize_variables_for_sumfinder(int_variables: Dict[VarName, VarValue], remove_zeros: bool = True, remove_duplicate_values: bool = True, max_value: int = None) -> Dict[VarName, VarValue]:

    optimized_variables = {var_id: var_val for var_id, var_val in int_variables.items()}

    if remove_zeros:
        # remove variables that have a value of 0 because they would result in an combination explosion in sumfinder as 0 + 0 = 0 and 0 + 0 + 0 = 0, etc.
        new_optimized_variables: Dict[VarName, VarValue] = {}
        for var_id, var_val in optimized_variables.items():
            if var_val != 0:
                new_optimized_variables[var_id] = var_val
            else:
                print(f'dropped variable {var_id} because its value is 0')
        optimized_variables = new_optimized_variables

    if max_value is not None:
        # remove variables which values are too big because the seem to cause huge slowdown
        new_optimized_variables: Dict[VarName, VarValue] = {}
        for var_id, var_val in optimized_variables.items():
            if var_val <= max_value:
                new_optimized_variables[var_id] = var_val
            else:
                print(f'dropped variable {var_id} because is value {var_val} exceeds {max_value}')
        optimized_variables = new_optimized_variables

    if remove_duplicate_values:
        # remove duplicate values because they would result in explosion of combinations and it's often because A contains B + C and C = 0
        new_optimized_variables: Dict[VarName, VarValue] = {}
        encountered_ints = set()
        for var_id, var_val in optimized_variables.items():
            if var_val not in encountered_ints:
                new_optimized_variables[var_id] = var_val
                encountered_ints.add(var_val)
            else:
                print(f'dropped variable {var_id} because another variable has the same value {var_val}')

        optimized_variables = new_optimized_variables

    return optimized_variables


def find_sums(variables: Dict[VarName, VarValue], sum_handler: ISumHandler):
    int_vars = [IntVariable(k, v) for k, v in variables.items()]
    int_vars.sort(key=lambda var: var.value, reverse=True)

    print(f'set of integer variables to explore: {int_vars}')
    for total_index in range(len(int_vars) - 1):
        find_numbers_summing_to(int_vars[total_index], int_vars[total_index + 1:], sum_handler)
    sum_handler.on_end()


def sums_to_dot(sum_file_path: Path, dot_file_path: Path):

    def to_dot_symbol(string: str) -> str:
        return string.replace('(', '_').replace(')', '_').replace('-', '_')

    # from https://graphviz.org/doc/info/colors.html
    svg_colors = [
        "aliceblue",
        "antiquewhite",
        "aqua",
        "aquamarine",
        "azure",
        "beige",
        "bisque",
        "black",
        "blanchedalmond",
        "blue",
        "blueviolet",
        "brown",
        "burlywood",
        "cadetblue",
        "chartreuse",
        "chocolate",
        "coral",
        "cornflowerblue",
        "cornsilk",
        "crimson",
        "cyan",
        "darkblue",
        "darkcyan",
        "darkgoldenrod",
        "darkgray",
        "darkgreen",
        "darkgrey",
        "darkkhaki",
        "darkmagenta",
        "darkolivegreen",
        "darkorange",
        "darkorchid",
        "darkred",
        "darksalmon",
        "darkseagreen",
        "darkslateblue",
        "darkslategray",
        "darkslategrey",
        "darkturquoise",
        "darkviolet",
        "deeppink",
        "deepskyblue",
        "dimgray",
        "dimgrey",
        "dodgerblue",
        "firebrick",
        "floralwhite",
        "forestgreen",
        "fuchsia",
        "gainsboro",
        "ghostwhite",
        "gold",
        "goldenrod",
        "gray",
        "grey",
        "green",
        "greenyellow",
        "honeydew",
        "hotpink",
        "indianred",
        "indigo",
        "ivory",
        "khaki",
        "lavender",
        "lavenderblush",
        "lawngreen",
        "lemonchiffon",
        "lightblue",
        "lightcoral",
        "lightcyan",
        "lightgoldenrodyellow",
        "lightgray",
        "lightgreen",
        "lightgrey",
        "lightpink",
        "lightsalmon",
        "lightseagreen",
        "lightskyblue",
        "lightslategray",
        "lightslategrey",
        "lightsteelblue",
        "lightyellow",
        "lime",
        "limegreen",
        "linen",
        "magenta",
        "maroon",
        "mediumaquamarine",
        "mediumblue",
        "mediumorchid",
        "mediumpurple",
        "mediumseagreen",
        "mediumslateblue",
        "mediumspringgreen",
        "mediumturquoise",
        "mediumvioletred",
        "midnightblue",
        "mintcream",
        "mistyrose",
        "moccasin",
        "navajowhite",
        "navy",
        "oldlace",
        "olive",
        "olivedrab",
        "orange",
        "orangered",
        "orchid",
        "palegoldenrod",
        "palegreen",
        "paleturquoise",
        "palevioletred",
        "papayawhip",
        "peachpuff",
        "peru",
        "pink",
        "plum",
        "powderblue",
        "purple",
        "red",
        "rosybrown",
        "royalblue",
        "saddlebrown",
        "salmon",
        "sandybrown",
        "seagreen",
        "seashell",
        "sienna",
        "silver",
        "skyblue",
        "slateblue",
        "slategray",
        "slategrey",
        "snow",
        "springgreen",
        "steelblue",
        "tan",
        "teal",
        "thistle",
        "tomato",
        "turquoise",
        "violet",
        "wheat",
        "white",
        "whitesmoke",
        "yellow",
        "yellowgreen"]

    print(f'creating {dot_file_path} from {sum_file_path}')
    sums = Sums.load(sum_file_path)
    # print(root_dict)
    # print(sums)
    with open(dot_file_path, 'wt+', encoding='utf8') as dot_file:
        dot_file.write(f'digraph {to_dot_symbol(sum_file_path.stem)}\n')
        dot_file.write('{\n')
        dot_file.write('layout = neato\n')
        dot_file.write('overlap = false\n')

        for variable in sums.variables:
            dot_file.write(f'{to_dot_symbol(variable.name)} [label="{variable.name}={variable.value}"];\n')
        sum_index = 0
        for s in sums.sums:
            dot_file.write(f"{to_dot_symbol(s['total'])} -> {{{','.join([to_dot_symbol(comp) for comp in s['components']])}}} [color={svg_colors[sum_index % len(svg_colors)]}, label={sum_index}];\n")
            sum_index += 1
        dot_file.write('}\n')


def find_and_graph_sums(variables: Dict[str, int], set_id: str):
    # set_id: eg 'set001' 'alambix97-meminfo'
    sums_file_path = Path(f'{set_id}-sums.json')
    dot_file_path = Path(f'{set_id}-sums.dot')
    svg_file_path = Path(f'{set_id}-sums.svg')
    sum_handler = SumExporter(sums_file_path)
    find_sums(variables, sum_handler)
    sums_to_dot(sums_file_path, dot_file_path)
    print(f'creating {svg_file_path} from {dot_file_path}')
    subprocess.run(f'dot -Tsvg {dot_file_path} > {svg_file_path}', shell=True, check=True)
