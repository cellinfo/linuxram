#!/usr/bin/env python3

import unittest
from pathlib import Path
from sumfinder import find_sums, SumExporter, Sums


def get_test_set_001():
    """a test for which c = a + b
    """
    return {
        'a': 3,
        'b': 8,
        'c': 11,
        'd': 5,
        'e': 1,
        'f': 2,
        'g': 7,
        'h': 7
    }


def get_test_set_002():
    """a test for which:
        c = a + b
        h = g + i
    """
    return {
        'a': 4,
        'b': 10,
        'c': 14,
        'd': 23,
        'e': 17,
        'f': 11,
        'g': 7,
        'h': 16,
        'i': 9
    }


class SumFinderTester(unittest.TestCase):

    def test_sumfinder1(self):
        sums1_path = Path('set001-sums.json')
        sums2_path = Path('set002-sums.json')
        find_sums(get_test_set_001(), SumExporter(sums1_path))
        find_sums(get_test_set_002(), SumExporter(sums2_path))
        sums1 = Sums.load(sums1_path)
        sums2 = Sums.load(sums2_path)
        if False:
            sums1.add_sums(sums2)
            valid_sums, invalid_sums = sums1.validate_on_variables(sums2.get_variables_as_dict())

            valid_sums.print()
        # merged_sums_path = Path('set001_002_sums.json')
        merged_sums = Sums.merge_sums([sums1, sums2])
        print('merged sums:')
        merged_sums.print()


if __name__ == "__main__":
    unittest.main(verbosity=1)
