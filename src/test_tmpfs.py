#!/usr/bin/env python3
# goal: find how tmpfs usage appears in /proc/meminfo

# from typing import Dict, List
# import re
# from datetime import datetime
from pathlib import Path
import subprocess
from linuxmem import get_stabilized_meminfo, meminfo_diff, KIBIBYTE_TO_BYTE, MIBIBYTE_TO_BYTE


class PollutedMeasure(Exception):
    pass


def measure_tmpfs_impact(file_size_in_mib: int):
    '''measures the effect of tmpfs file on meminfo variables
    '''
    stresser_file_path = Path('/dev/shm/tmpfs-stresser.bin')
    if stresser_file_path.exists():
        stresser_file_path.unlink()

    ramana_out_root = Path('./ramana-out')
    ramana_out_root.mkdir(exist_ok=True)

    tolerance = 16 * 1024 * KIBIBYTE_TO_BYTE

    measurement_succeded = False

    changed_vars = {}
    while not measurement_succeded:
        try:
            changed_vars = {}
            # meminfo1_file_path = ramana_out_root / f'meminfo-{datetime.now().isoformat()}.stdout'
            # subprocess.run(f'cat /proc/meminfo > {meminfo1_file_path}', shell=True, check=True, capture_output=True)
            # meminfo1 = read_meminfo_stdout(meminfo1_file_path)
            meminfo1 = get_stabilized_meminfo(tolerance)

            file_size_in_mib = 200

            subprocess.run(f'dd if=/dev/random bs=1MiB count={file_size_in_mib} of={stresser_file_path}', shell=True, check=True, capture_output=True)

            subprocess.run('sleep 1', shell=True, check=True, capture_output=True)

            meminfo2 = get_stabilized_meminfo(tolerance)

            diff = meminfo_diff(meminfo1, meminfo2)
            for var_id, size_change in diff.items():
                if abs(abs(size_change) - file_size_in_mib * MIBIBYTE_TO_BYTE) < tolerance:
                    changed_vars[var_id] = {
                        'size_before_alloc': meminfo1[var_id],
                        'size_after_alloc': meminfo2[var_id]
                    }
                elif abs(abs(size_change)) > tolerance:  # the rest of the variables are expected to be unaffected (ie size_change shoud be close to zero)
                    raise PollutedMeasure()
                    # print(f'unexpected change for {var_id}: ({size_change} bytes: {meminfo1[var_id]} bytes -> {meminfo2[var_id]} bytes)')
            measurement_succeded = True
            stresser_file_path.unlink(meminfo1)

        except PollutedMeasure:
            print('warning: measurement was polluted by other processes. Starting again...')
            stresser_file_path.unlink(meminfo1)

    print(f'effect of the creation of the tmps file {stresser_file_path} of {file_size_in_mib} mib ({file_size_in_mib * MIBIBYTE_TO_BYTE} bytes):')
    for var_id, var_vals in changed_vars.items():
        size_change = var_vals['size_after_alloc'] - var_vals['size_before_alloc']
        change_direction = {
            False: 'decrease',
            True: 'increase'
        }[size_change > 0]
        print(f'({change_direction}) of {var_id} ({size_change} bytes: {meminfo1[var_id]} bytes -> {meminfo2[var_id]} bytes)')

    # print(f'variables that changed of {file_size_in_mib} mib')
    # print(diff)


def main():
    measure_tmpfs_impact(200)


main()
