#!/usr/bin/env python3
# this tool is used to find the sums in the variables output by /proc/meminfo
from argparse import ArgumentParser
from pathlib import Path
from sumfinder import find_and_graph_sums, optimize_variables_for_sumfinder
from linuxmem import read_meminfo_stdout


def str2bool(s):
    return str(s).lower() == 'true'


def main():
    arg_parser = ArgumentParser(description='finds sums in meminfo variables')
    arg_parser.add_argument('--meminfo-stdout-file', type=Path, required=True, help='the path of the input file containing the output of `cat /proc/meminfo`')
    arg_parser.add_argument('--id', required=True, help='used as a prefix for output files')
    arg_parser.add_argument('--remove-zeros', type=str2bool, required=True, help='if true, removes the variables that have a value of zero (variables withe zero value might cause a massive increase of sums due to combination explosition)')
    arg_parser.add_argument('--remove-duplicate-values', type=str2bool, required=True, help='if true, removes duplicates to ensure each variable have a unique value (values with more than one variable might cause a massive increase of sums due to combination explosition)')
    arg_parser.add_argument('--max-value', type=int, help='if set, removes the variable which value exceeds this (for some reason, huge values have been known to dramatically slow down the sum finder)')
    args = arg_parser.parse_args()
    max_value = None
    if args.max_value:
        max_value = args.max_value
    meminfo_vars = read_meminfo_stdout(args.meminfo_stdout_file)
    add_extra_meminfo_vars = True
    if add_extra_meminfo_vars:
        meminfo_vars['MemTotalPlusSwapTotal'] = meminfo_vars['MemTotal'] + meminfo_vars['SwapTotal']
        # wtf: the following equation seems to indicate that SwapFree is included into MemTotal ...?
        # sum 0 found: MemTotalPlusSwapTotal = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
        # maybe it's just a coincidence but it's worth adding MemTotalMinusSwapTotal
        meminfo_vars['MemTotalMinusSwapTotal'] = meminfo_vars['MemTotal'] - meminfo_vars['SwapTotal']
        meminfo_vars['MemTotalPlusHardwareCorrupted'] = meminfo_vars['MemTotal'] + meminfo_vars['HardwareCorrupted']
        meminfo_vars['MemTotalMinusHardwareCorrupted'] = meminfo_vars['MemTotal'] - meminfo_vars['HardwareCorrupted']
        meminfo_vars['MemTotalPlusSwapTotalPlusHardwareCorrupted'] = meminfo_vars['MemTotalPlusSwapTotal'] + meminfo_vars['HardwareCorrupted']
        meminfo_vars['MemTotalPlusSwapTotalMinusHardwareCorrupted'] = meminfo_vars['MemTotalPlusSwapTotal'] - meminfo_vars['HardwareCorrupted']
    find_and_graph_sums(optimize_variables_for_sumfinder(meminfo_vars, remove_zeros=args.remove_zeros, remove_duplicate_values=args.remove_duplicate_values, max_value=max_value), args.id)


main()
