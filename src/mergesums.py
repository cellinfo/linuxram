#!/usr/bin/env python3
# this tool is used to find the sums in the variables output by /proc/meminfo
from argparse import ArgumentParser
from pathlib import Path
from sumfinder import Sums


def str2bool(s):
    return str(s).lower() == 'true'


def main():
    arg_parser = ArgumentParser(description='merge sums together')
    arg_parser.add_argument('--input-sums-files', type=Path, nargs='+', required=True, help='the path of the input sums files containing (in json format)')
    arg_parser.add_argument('--output-sums-file', type=Path, required=True, help='the path of the output sums file (which will contained a fusion of all input sums)')
    args = arg_parser.parse_args()
    print(args.input_sums_files)
    input_sums = [Sums.load(f) for f in args.input_sums_files]
    merged_sums = Sums.merge_sums(input_sums)
    merged_sums.print()
    Sums.save(merged_sums, args.output_sums_file)


main()
