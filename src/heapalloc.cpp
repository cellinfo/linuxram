#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

void simple_heap_allocation(size_t bufferSize, bool fillBuffer)
{
	char* pBuffer = static_cast<char*>(malloc(bufferSize));
	if (pBuffer == 0)
	{
		cout << "failed to allocate " << bufferSize << " bytes" << endl;
		exit(1);
	}
	cout << "allocated a buffer of " << bufferSize << " bytes on the heap" << endl;
	if (fillBuffer)
	{
		for (size_t i = 0; i < bufferSize; ++i)
		{
			pBuffer[i] = i % 0xff;
		}
		cout << "filled buffer with see saw signal" << endl;
	}
	while(true)
	{
		cout << "press ctrl(c) to terminate the program" << endl;
		sleep(3);
	}
	free(pBuffer);
	cout << "freed the buffer of " << bufferSize << " bytes" << endl;
}

void printUsage(const char* pExeName)
{
	cout << "Usage: " << endl;
	cout << endl;
	cout << pExeName << " <fill-buffer|no-fill-buffer>" << endl;
}

int main(int argc, char* argv[])
{
	size_t bufferSize = 1024 * 1024 * 1024 * sizeof(unsigned char);
	bool fillBuffer = false;

	if(argc < 2)
	{
		printUsage(argv[0]);
		exit(1);
	}

	string arg1 = argv[1];
	if (arg1 == "fill-buffer")
	{
		fillBuffer = true;
	}
	else if(arg1 == "no-fill-buffer")
	{
		fillBuffer = false;
	}
	else
	{
		printUsage(argv[0]);
		exit(1);
	}
	simple_heap_allocation(bufferSize, fillBuffer);
}
