#!/usr/bin/env python3
# given a list of integer variables and their values, this tool aims at finding which varibales can be obtained by the sum of others
# note : this tool is used to find the sums in the variables output by /proc/meminfo
from sumfinder import find_and_graph_sums


def get_test_set_002():
    # alambix97/20240805-191606-meminfo.stdout
    return {'MemTotal': 201214205952, 'MemFree': 803061760, 'MemAvailable': 131466833920, 'Buffers': 31981568, 'Cached': 130939863040, 'SwapCached': 1088724992, 'Active': 103304773632, 'Inactive': 94177337344, 'Active(anon)': 39671312384, 'Inactive(anon)': 26918952960, 'Active(file)': 63633461248, 'Inactive(file)': 67258384384, 'Unevictable': 23019520, 'Mlocked': 19873792, 'SwapTotal': 32765898752, 'Dirty': 8546779136, 'Writeback': 5550080, 'AnonPages': 65442127872, 'Mapped': 120565760, 'Shmem': 69955584, 'KReclaimable': 1252827136, 'Slab': 1991671808, 'SReclaimable': 1252827136, 'SUnreclaim': 738844672, 'KernelStack': 17088512, 'PageTables': 218902528, 'CommitLimit': 133373001728, 'Committed_AS': 141631254528, 'VmallocTotal': 35184372087808, 'VmallocUsed': 302333952, 'Percpu': 215089152, 'HardwareCorrupted': 53248, 'AnonHugePages': 49218060288, 'Hugepagesize': 2097152, 'DirectMap4k': 554131456, 'DirectMap2M': 6434062336, 'DirectMap1G': 199715979264}


def get_test_set_003():
    # same as test_set_002 but without VmallocTotal which for some reason causes the program to become extremeley slow (maybe because it's a huge integer ?)
    return {'MemTotal': 201214205952, 'MemFree': 803061760, 'MemAvailable': 131466833920, 'Buffers': 31981568, 'Cached': 130939863040, 'SwapCached': 1088724992, 'Active': 103304773632, 'Inactive': 94177337344, 'Active(anon)': 39671312384, 'Inactive(anon)': 26918952960, 'Active(file)': 63633461248, 'Inactive(file)': 67258384384, 'Unevictable': 23019520, 'Mlocked': 19873792, 'SwapTotal': 32765898752, 'Dirty': 8546779136, 'Writeback': 5550080, 'AnonPages': 65442127872, 'Mapped': 120565760, 'Shmem': 69955584, 'KReclaimable': 1252827136, 'Slab': 1991671808, 'SReclaimable': 1252827136, 'SUnreclaim': 738844672, 'KernelStack': 17088512, 'PageTables': 218902528, 'CommitLimit': 133373001728, 'Committed_AS': 141631254528, 'VmallocUsed': 302333952, 'Percpu': 215089152, 'HardwareCorrupted': 53248, 'AnonHugePages': 49218060288, 'Hugepagesize': 2097152, 'DirectMap4k': 554131456, 'DirectMap2M': 6434062336, 'DirectMap1G': 199715979264}


def main():
    find_and_graph_sums(get_test_set_003(), 'alambix97-meminfo')


main()
