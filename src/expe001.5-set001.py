#!/usr/bin/env python3
from sumfinder import find_and_graph_sums


def get_test_set_001():
    return {
        'a': 3,
        'b': 8,
        'c': 11,
        'd': 5,
        'e': 1,
        'f': 2,
        'g': 6,
        'h': 7
    }


def main():
    find_and_graph_sums(get_test_set_001(), 'set001')


main()
