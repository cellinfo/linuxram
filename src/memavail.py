#!/usr/bin/env python3
"""
code to userstand how meminfo's MemAvailable is computed

how to use:

```sh
20240813-16:43:33 graffy@graffy-ws2:~/work/linuxram.git$ export LINUXRAM_ROOT=$(pwd)
20240813-16:48:20 graffy@graffy-ws2:~/work/linuxram.git$ ./src/test_tmpfs.py
```

"""
from pathlib import Path
import os
from linuxmem import compute_mem_avail


def main():

    set_id = 'stressed_alambix97'
    # set_id = 'validation_alambix98'
    linuxram_root_dir = Path(os.getenv('LINUXRAM_ROOT'))

    if set_id == 'stressed_alambix97':
        meminfo_stdout_file_path = linuxram_root_dir / 'measurements/bug3897/alambix97/20240805-191606-meminfo.stdout'
        grepped_zoneinfo_file_path = linuxram_root_dir / 'measurements/bug3897/alambix98/20240806-202043-grepped-zoneinfo.stdout'  # I didn't have a chance to get zone info before it crashed so I used another measure, hoping that zoneinfo is minimal
    elif set_id == 'validation_alambix98':
        meminfo_stdout_file_path = linuxram_root_dir / 'measurements/alambix98/20240806-223916-meminfo.stdout'
        grepped_zoneinfo_file_path = linuxram_root_dir / 'measurements/bug3897/alambix98/20240806-223916-grepped-zoneinfo.stdout'

    compute_mem_avail(meminfo_stdout_file_path, grepped_zoneinfo_file_path)


main()
