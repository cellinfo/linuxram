experimentations to measure the impact of typical heap memory allocations on meminfo indicators (basicall which meminfo indicators record heap memory allocations)

# 09/08/2024

## expe001

20240809-19:08:13 graffy@graffy-ws2:~/work/tickets/bug3897$ rsync -va ./expe001/ graffy@alambix.ipr.univ-rennes.fr:/mnt/home.ipr/graffy/tickets/bug3897/expe001/
sending incremental file list
created directory /mnt/home.ipr/graffy/tickets/bug3897/expe001
./
expe001.cpp

sent 861 bytes  received 105 bytes  386.40 bytes/sec
total size is 727  speedup is 0.75
last command status : [0]

graffy@alambix-frontal:~/tickets/bug3897/expe001$ date; ./a.out ; date
ven. 09 août 2024 19:23:13 CEST
allocated a buffer of 1073741824 bytes on the heap
press ctrl(c) to terminate the program
press ctrl(c) to terminate the program
press ctrl(c) to terminate the program
^C
graffy@alambix-frontal:~/tickets/bug3897/expe001$ date
ven. 09 août 2024 19:23:21 CEST
graffy@alambix-frontal:~/tickets/bug3897/expe001$ 


graffy@alambix-frontal:~/tickets/bug3897/expe001$ while true; do date; cat /proc/meminfo > meminfo-dumps/meminfo-$(date --iso=seconds).stdout; sleep 1; done
ven. 09 août 2024 19:23:10 CEST
ven. 09 août 2024 19:23:11 CEST
ven. 09 août 2024 19:23:12 CEST
ven. 09 août 2024 19:23:13 CEST
ven. 09 août 2024 19:23:14 CEST
ven. 09 août 2024 19:23:15 CEST
ven. 09 août 2024 19:23:16 CEST
ven. 09 août 2024 19:23:17 CEST
ven. 09 août 2024 19:23:18 CEST
ven. 09 août 2024 19:23:19 CEST
ven. 09 août 2024 19:23:20 CEST
ven. 09 août 2024 19:23:21 CEST
ven. 09 août 2024 19:23:23 CEST
ven. 09 août 2024 19:23:24 CEST
^C

-> only AS_Commited has increased (1 Gb)

test 2

```sh
graffy@alambix-frontal:~/tickets/bug3897/expe001$ date; ./a.out fill-buffer; date
ven. 09 août 2024 19:47:21 CEST
allocated a buffer of 1073741824 bytes on the heap
filled buffer with see saw signal
press ctrl(c) to terminate the program
press ctrl(c) to terminate the program
press ctrl(c) to terminate the program
^C
graffy@alambix-frontal:~/tickets/bug3897/expe001$ date
ven. 09 août 2024 19:47:35 CEST
```

```sh
graffy@alambix-frontal:~/tickets/bug3897/expe001$ while true; do date; cat /proc/meminfo > meminfo-dumps/meminfo-$(date --iso=seconds).stdout; sleep 1; done
ven. 09 août 2024 19:47:17 CEST
ven. 09 août 2024 19:47:18 CEST
ven. 09 août 2024 19:47:19 CEST
ven. 09 août 2024 19:47:20 CEST
ven. 09 août 2024 19:47:21 CEST
ven. 09 août 2024 19:47:22 CEST
ven. 09 août 2024 19:47:23 CEST
ven. 09 août 2024 19:47:24 CEST
ven. 09 août 2024 19:47:25 CEST
ven. 09 août 2024 19:47:26 CEST
ven. 09 août 2024 19:47:28 CEST
ven. 09 août 2024 19:47:29 CEST
ven. 09 août 2024 19:47:30 CEST
ven. 09 août 2024 19:47:31 CEST
ven. 09 août 2024 19:47:32 CEST
ven. 09 août 2024 19:47:33 CEST
ven. 09 août 2024 19:47:34 CEST
ven. 09 août 2024 19:47:35 CEST
ven. 09 août 2024 19:47:36 CEST
ven. 09 août 2024 19:47:37 CEST
ven. 09 août 2024 19:47:38 CEST
^C
```


```sh
20240809-19:44:50 graffy@graffy-ws2:~/work/tickets/bug3897$ rsync -va graffy@alambix.ipr.univ-rennes.fr:/mnt/home.ipr/graffy/tickets/bug3897/expe001/ ./expe001/
receiving incremental file list
./
a.out
meminfo-dumps/
meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout
meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout

sent 601 bytes  received 51,307 bytes  34,605.33 bytes/sec
total size is 78,433  speedup is 1.51
last command status : [0]
20240809-19:48:09 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'Inactive(anon)' ./expe001/
a.out          expe001.cpp    meminfo-dumps/ 
last command status : [0]
20240809-19:48:09 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'Inactive(anon)' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:Inactive(anon):   171788 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:Inactive(anon):   172092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:Inactive(anon):   171912 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:Inactive(anon):   171964 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:Inactive(anon):   170120 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:Inactive(anon):   170280 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:Inactive(anon):   170536 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:Inactive(anon):   170544 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:Inactive(anon):   170176 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:Inactive(anon):   170244 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:Inactive(anon):   170608 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:Inactive(anon):   169976 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:Inactive(anon):   169964 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:Inactive(anon):   170004 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:Inactive(anon):   155168 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:Inactive(anon):   155104 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:Inactive(anon):   155312 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:Inactive(anon):   155676 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:Inactive(anon):   155676 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:Inactive(anon):   287176 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:Inactive(anon):   573912 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:Inactive(anon):   870788 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:Inactive(anon):  1163824 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:Inactive(anon):  1204000 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:Inactive(anon):  1203968 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:Inactive(anon):  1204236 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:Inactive(anon):  1203932 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:Inactive(anon):  1203840 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:Inactive(anon):  1203940 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:Inactive(anon):  1204204 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:Inactive(anon):   155324 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:Inactive(anon):   155500 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:Inactive(anon):   155864 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:Inactive(anon):   155864 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:Inactive(anon):   155864 kB
last command status : [0]
20240809-19:52:36 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'Active(anon)' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:Active(anon):        700 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:Active(anon):        700 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:Active(anon):        704 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:Active(anon):        700 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:Active(anon):        704 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:Active(anon):        704 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:Active(anon):        700 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:Active(anon):        704 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:Active(anon):        700 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:Active(anon):        704 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:Active(anon):        704 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:Active(anon):        700 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:Active(anon):        696 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:Active(anon):        696 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:Active(anon):        628 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:Active(anon):        628 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:Active(anon):        628 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:Active(anon):        636 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:Active(anon):        636 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:Active(anon):        636 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:Active(anon):        636 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:Active(anon):        636 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:Active(anon):        632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:Active(anon):        632 kB
last command status : [0]
20240809-19:52:47 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'MemAvailable' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:MemAvailable:   64545808 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:MemAvailable:   64545560 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:MemAvailable:   64545960 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:MemAvailable:   64545676 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:MemAvailable:   64548360 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:MemAvailable:   64548584 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:MemAvailable:   64548704 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:MemAvailable:   64548084 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:MemAvailable:   64548436 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:MemAvailable:   64548644 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:MemAvailable:   64548504 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:MemAvailable:   64548456 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:MemAvailable:   64548908 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:MemAvailable:   64548356 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:MemAvailable:   64565760 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:MemAvailable:   64565896 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:MemAvailable:   64565520 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:MemAvailable:   64565076 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:MemAvailable:   64564936 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:MemAvailable:   64433964 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:MemAvailable:   64146260 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:MemAvailable:   63846540 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:MemAvailable:   63554988 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:MemAvailable:   63512904 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:MemAvailable:   63513024 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:MemAvailable:   63512524 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:MemAvailable:   63513344 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:MemAvailable:   63513076 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:MemAvailable:   63513556 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:MemAvailable:   63518836 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:MemAvailable:   64570328 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:MemAvailable:   64570824 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:MemAvailable:   64572280 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:MemAvailable:   64571776 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:MemAvailable:   64572504 kB
last command status : [0]
20240809-19:53:07 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'Commited_AS' ./expe001/meminfo-dumps/*
last command status : [0]
20240809-19:54:04 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'Committed_AS' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:Committed_AS:     439080 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:Committed_AS:     439080 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:Committed_AS:     439080 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:Committed_AS:     439080 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:Committed_AS:    1484492 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:Committed_AS:    1484492 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:Committed_AS:    1484492 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:Committed_AS:    1484492 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:Committed_AS:    1484492 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:Committed_AS:     435440 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:Committed_AS:     435440 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:Committed_AS:     435440 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:Committed_AS:     435440 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:Committed_AS:     435440 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:Committed_AS:    1459144 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:Committed_AS:     410092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:Committed_AS:     410092 kB
last command status : [0]
20240809-19:54:11 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'AnonPages' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:AnonPages:        182172 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:AnonPages:        182416 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:AnonPages:        182276 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:AnonPages:        182296 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:AnonPages:        180536 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:AnonPages:        180576 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:AnonPages:        180856 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:AnonPages:        180884 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:AnonPages:        180492 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:AnonPages:        180576 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:AnonPages:        180940 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:AnonPages:        180300 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:AnonPages:        180284 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:AnonPages:        180352 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:AnonPages:        165480 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:AnonPages:        165376 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:AnonPages:        165564 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:AnonPages:        165928 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:AnonPages:        165928 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:AnonPages:        297504 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:AnonPages:        584224 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:AnonPages:        881096 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:AnonPages:       1174132 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:AnonPages:       1214280 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:AnonPages:       1214248 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:AnonPages:       1214484 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:AnonPages:       1214240 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:AnonPages:       1214156 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:AnonPages:       1214216 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:AnonPages:       1214452 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:AnonPages:        165564 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:AnonPages:        165716 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:AnonPages:        166080 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:AnonPages:        166080 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:AnonPages:        166080 kB
last command status : [0]
20240809-19:54:35 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'AnonHugePages' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:AnonHugePages:    217088 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:AnonHugePages:    503808 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:AnonHugePages:    800768 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:AnonHugePages:   1093632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:AnonHugePages:   1132544 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:AnonHugePages:   1132544 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:AnonHugePages:   1132544 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:AnonHugePages:   1132544 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:AnonHugePages:   1132544 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:AnonHugePages:   1132544 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:AnonHugePages:   1132544 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:AnonHugePages:     86016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:AnonHugePages:     86016 kB
last command status : [0]
20240809-19:55:00 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'MemFree' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:MemFree:        54511404 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:MemFree:        54511152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:MemFree:        54511548 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:MemFree:        54511260 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:MemFree:        54513940 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:MemFree:        54514156 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:MemFree:        54514276 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:MemFree:        54513656 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:MemFree:        54513996 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:MemFree:        54514200 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:MemFree:        54514060 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:MemFree:        54514004 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:MemFree:        54514452 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:MemFree:        54513896 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:MemFree:        54533156 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:MemFree:        54533288 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:MemFree:        54532912 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:MemFree:        54532468 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:MemFree:        54532328 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:MemFree:        54401340 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:MemFree:        54113632 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:MemFree:        53813908 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:MemFree:        53522352 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:MemFree:        53480264 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:MemFree:        53480384 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:MemFree:        53479880 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:MemFree:        53480692 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:MemFree:        53480420 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:MemFree:        53480896 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:MemFree:        53486172 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:MemFree:        54537660 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:MemFree:        54538152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:MemFree:        54539608 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:MemFree:        54539104 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:MemFree:        54539832 kB
last command status : [0]
20240809-19:55:42 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'Inactive:' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:Inactive:        2013456 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:Inactive:        2013764 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:Inactive:        2013588 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:Inactive:        2013644 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:Inactive:        2011804 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:Inactive:        2011972 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:Inactive:        2012228 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:Inactive:        2012236 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:Inactive:        2011880 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:Inactive:        2011952 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:Inactive:        2012316 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:Inactive:        2011692 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:Inactive:        2011684 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:Inactive:        2011728 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:Inactive:        1973328 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:Inactive:        1973268 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:Inactive:        1973476 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:Inactive:        1973840 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:Inactive:        1973840 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:Inactive:        2105356 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:Inactive:        2392096 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:Inactive:        2688976 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:Inactive:        2982016 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:Inactive:        3022196 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:Inactive:        3022164 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:Inactive:        3022436 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:Inactive:        3022140 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:Inactive:        3022052 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:Inactive:        3022156 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:Inactive:        3022424 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:Inactive:        1973548 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:Inactive:        1973728 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:Inactive:        1974092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:Inactive:        1974092 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:Inactive:        1974092 kB
last command status : [0]
20240809-19:56:32 graffy@graffy-ws2:~/work/tickets/bug3897$ grep 'Cached:' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:Cached:          8908952 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:Cached:          8908956 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:Cached:          8908964 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:Cached:          8908968 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:Cached:          8908972 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:Cached:          8908976 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:Cached:          8908976 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:Cached:          8908976 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:Cached:          8908988 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:Cached:          8908992 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:Cached:          8908992 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:Cached:          8909000 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:Cached:          8909004 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:Cached:          8909008 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:Cached:          8908148 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:Cached:          8908152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:Cached:          8908152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:Cached:          8908152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:Cached:          8908152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:Cached:          8908168 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:Cached:          8908172 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:Cached:          8908176 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:Cached:          8908180 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:Cached:          8908184 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:Cached:          8908184 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:Cached:          8908188 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:Cached:          8908196 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:Cached:          8908200 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:Cached:          8908204 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:Cached:          8908208 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:Cached:          8908212 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:Cached:          8908216 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:Cached:          8908216 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:Cached:          8908216 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:SwapCached:            0 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:Cached:          8908216 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:SwapCached:            0 kB
last command status : [0]
20240809-19:57:10 graffy@graffy-ws2:~/work/tickets/bug3897$ grep '^Cached:' ./expe001/meminfo-dumps/*
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:10+02:00.stdout:Cached:          8908952 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:11+02:00.stdout:Cached:          8908956 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:12+02:00.stdout:Cached:          8908964 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:13+02:00.stdout:Cached:          8908968 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:14+02:00.stdout:Cached:          8908972 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:15+02:00.stdout:Cached:          8908976 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:16+02:00.stdout:Cached:          8908976 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:17+02:00.stdout:Cached:          8908976 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:18+02:00.stdout:Cached:          8908988 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:19+02:00.stdout:Cached:          8908992 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:20+02:00.stdout:Cached:          8908992 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:21+02:00.stdout:Cached:          8909000 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:23+02:00.stdout:Cached:          8909004 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:23:24+02:00.stdout:Cached:          8909008 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:17+02:00.stdout:Cached:          8908148 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:18+02:00.stdout:Cached:          8908152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:19+02:00.stdout:Cached:          8908152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:20+02:00.stdout:Cached:          8908152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:21+02:00.stdout:Cached:          8908152 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:22+02:00.stdout:Cached:          8908168 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:23+02:00.stdout:Cached:          8908172 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:24+02:00.stdout:Cached:          8908176 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:25+02:00.stdout:Cached:          8908180 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:26+02:00.stdout:Cached:          8908184 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:28+02:00.stdout:Cached:          8908184 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:29+02:00.stdout:Cached:          8908188 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:30+02:00.stdout:Cached:          8908196 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:31+02:00.stdout:Cached:          8908200 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:32+02:00.stdout:Cached:          8908204 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:33+02:00.stdout:Cached:          8908208 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:34+02:00.stdout:Cached:          8908212 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:35+02:00.stdout:Cached:          8908216 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:36+02:00.stdout:Cached:          8908216 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:37+02:00.stdout:Cached:          8908216 kB
./expe001/meminfo-dumps/meminfo-2024-08-09T19:47:38+02:00.stdout:Cached:          8908216 kB
last command status : [0]
```

-> c'est intéressant:
- sans remplissage du buffer, seul AS_Committed a changé
- avec remplissage du buffer, c'est Inactive(anon) qui monte de 1G, et  MemFree, MemAvailable bougent aussi

-> il serait intéressant d'automatiser ce test en python afin de:
1. détecter automatiquement ce qui change (avec une tolérance)
2. grapher les évolutions
3. inclure des test tmpfs

# 13/08/2024

renamed expe001.cpp as heapalloc.cpp so that it could be reused for other experimentations (measurements)
