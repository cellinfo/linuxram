
## try to figure out a map from [text](src/sumfinder/simplification/treemap000.md)

# 14/08/2024

### map2

[^10] shows that <tmpfs> is also accounted in `Inactive(anon)` and `Inactive`, which leads me to think of an alternate segmentation (segmentation2)
```md
- 196498248 kB `MemTotal`
   - 100883568 kB `Active`
	  -  38741516 kB `Active(anon)`
	  -  62142052 kB `Active(file)`
   -  91970056 kB `Inactive`
	  -  26288040 kB `Inactive(anon)`
	  -  65682016 kB `Inactive(file)`
   -   3644624 kB <unidentified-rest-2>
```

[^tmpfs_computation]: we know from [^8] and [^6] that `Cache` = `pagecache` + `tmpfs`. So in the case of [text](measurements/bug3897/alambix97/20240805-191606-meminfo.stdout):

`tmpfs` = `Cache` - `pagecache`
`tmpfs` = 127870960 kB - 127824068 kB
`tmpfs` = 46892 kB

note: we know from [^6] that `Shmem` = `tmpfs` + `shared-mem`. So in the case of [text](measurements/bug3897/alambix97/20240805-191606-meminfo.stdout), where `Shmem` = 68316 kB, we can check that `Shmem` > `tmpfs`, and therefore `shared-mem` has a positive value, which is reassuring.


### map3: MemTotal = Active + Inactive + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted

This equation (equation 21 from [[text](../expe001.5/simplification/treemap000.md)]) was randomly picked from sumfinder using [text](measurements/bug3897/alambix97/20240805-191606-meminfo.stdout).

```md
- 196498248 kB `MemTotal` (segmentation1)
   - 100883568 kB `Active`
	  -  62142052 kB `Active(file)`
	  -  38741516 kB `Active(anon)`
   -  91970056 kB `Inactive`
	  -  65682016 kB `Inactive(file)`
	  -  26288040 kB `Inactive(anon)`
   -   1944992 kB `Slab`
	  -   1223464 kB `KReclaimable`
		 -   1223464 kB `SReclaimable`
		 -         0 kB <other-direct-allocations>
	  -    721528 kB `SUnreclaim`
   -   1063208 kB `SwapCached`
   -    541144 kB `DirectMap4k`
   -     31232 kB `Buffers`
   -     22480 kB `Unevictable`
   -     19408 kB `Mlocked`
   -     16688 kB `KernelStack`
   -      5420 kB `Writeback`
   -        52 kB `HardwareCorrupted`
```

check:
```py
>>> 196498248 - (100883568 + 91970056 + 1944992 + 1063208 + 541144 + 31232 + 22480 + 19408 + 16688 + 5420 + 52)
0
```
-> problem:where is MemFree (784240 kB) in this picture, as from [^11], MemFree is on physical ram ?


### map4

choose another equation from [text](measurements/bug3897/alambix97/20240805-191606-meminfo.stdout).

#### simplification 1

-> filter equations with MemTotal = 
-> removed equations without MemFree
-> removed equations without SwapCached

1. MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
3. MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted

2. MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
4. MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack

11. MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
13. MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted

12. MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
14. MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack

23. MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
24. MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted

#### simplification 2

-> removed doublons equations with KReclaimable because KReclaimable=SReclaimable  and KReclaimable is supposed to contain SReclaimable

1. MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted

2. MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack

11. MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted

12. MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack

23. MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted

#### simplification 3

-> removed equation 23 because it contains SwapTotal

1. MemTotal = Group3 + Group1

2. MemTotal = Group3 + Group2

11. MemTotal = Group4 + Group1

12. MemTotal = Group4 + Group2

with:
Group1 = VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
Group2 = DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
Group1 = Group2
Group3 = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim
Group4 = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim
Group3 = Group4

#### simplification 4

-> to simplify, removed Group3 because Group3 = Group4 is expected because Active = Active(file) + Active(anon)

This leaves us with 

11. MemTotal = Group4 + Group1

12. MemTotal = Group4 + Group2

with:
Group1 = VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
Group2 = DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
Group1 = Group2
Group4 = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim

#### simplification 5

-> compare Group1 and Group2
Group1 =               VmallocUsed                + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
Group2 = DirectMap4k + VmallocUsed + Unevictable                                           + Mlocked                                                + KernelStack

Group1 = Group5 + Group6
Group2 = Group5 + Group7

with:
Group5 = VmallocUsed + Mlocked
Group6 = PageTables + Percpu + Mapped + Buffers + Writeback + Hugepagesize + HardwareCorrupted
Group7 = DirectMap4k + Unevictable + KernelStack
Group6 = Group7

#### is it a coincidence that Group6 = Group7 ?

Check with [text](measurements/bug3897/alambix98/20240806-223916-meminfo.stdout)

PageTables:        66404 kB
Percpu:           229632 kB
Mapped:           143340 kB
Buffers:          843076 kB
Writeback:             0 kB
Hugepagesize:       2048 kB
HardwareCorrupted:     0 kB
---------------------------
Group6           1284500 kB

DirectMap4k:      551400 kB
Unevictable:       22480 kB
KernelStack:       17408 kB
---------------------------
Group7            591288 kB

-> ok: this means that Group6 = Group7 is a coincidence

-> I tend to prefer to beleive that Group6 is the right one, but this needs to be checked though


### sorted [text](measurements/bug3897/alambix97/20240805-191606-meminfo.stdout):

```s
VmallocTotal: 34359738367 kB
MemTotal:       196498248 kB
DirectMap1G:    195035136 kB
Committed_AS:   138311772 kB
CommitLimit:    130247072 kB
MemAvailable:   128385580 kB
Cached:         127870960 kB
Active:         100883568 kB
Inactive:        91970056 kB
Inactive(file):  65682016 kB
AnonPages:       63908328 kB
Active(file):    62142052 kB
AnonHugePages:   48064512 kB
Active(anon):    38741516 kB
Inactive(anon):  26288040 kB
SwapTotal:       31997948 kB
Dirty:            8346464 kB
DirectMap2M:      6283264 kB
Slab:             1944992 kB
KReclaimable:     1223464 kB
SReclaimable:     1223464 kB
SwapCached:       1063208 kB
MemFree:           784240 kB
SUnreclaim:        721528 kB
DirectMap4k:       541144 kB
VmallocUsed:       295248 kB
PageTables:        213772 kB
Percpu:            210048 kB
Mapped:            117740 kB
Buffers:            31232 kB
Unevictable:        22480 kB
Mlocked:            19408 kB
Writeback:           5420 kB
Shmem:              68316 kB
KernelStack:        16688 kB
Hugepagesize:        2048 kB
HardwareCorrupted:     52 kB
```


## filter out the sums that are not valid for other meminfo stdout

In order to remove the concidences in the sums found

```sh
20240814-18:22:58 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe003$ ../../src/expe003.py 
starting sums (90 sums):
0: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
1: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
2: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
3: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
4: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
5: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
6: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
7: MemTotal = Inactive + Active(file) + Active(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
8: MemTotal = Inactive + Active(file) + Active(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
9: MemTotal = Inactive + Active(file) + Active(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
10: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
11: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
12: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
13: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
14: MemTotal = Active + AnonHugePages + Active(anon) + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
15: MemTotal = Active + AnonHugePages + Active(anon) + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
16: MemTotal = Active + Inactive(file) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
17: MemTotal = Active + Inactive(file) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
18: MemTotal = Active + Inactive(file) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
19: MemTotal = Active + Inactive + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
20: MemTotal = Active + Inactive + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
21: MemTotal = Active + Inactive + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
22: MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
23: MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
24: MemTotal = Cached + SwapTotal + Inactive(anon) + Dirty + SwapCached + SUnreclaim + Percpu + HardwareCorrupted
25: MemTotal = Cached + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
26: MemTotal = Cached + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
27: MemTotal = Cached + Active(anon) + Inactive(anon) + Slab + SwapCached + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
28: MemTotal = MemAvailable + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SReclaimable + SwapCached + Buffers + Hugepagesize
29: MemTotal = CommitLimit + Active(anon) + Inactive(anon) + SUnreclaim + VmallocUsed + Mapped + Shmem + KernelStack + Hugepagesize + HardwareCorrupted
30: DirectMap1G = AnonPages + Active(file) + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + SReclaimable + MemFree + DirectMap4k + VmallocUsed + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
31: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
32: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
33: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + Slab + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
34: DirectMap1G = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
35: DirectMap1G = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
36: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + SReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
37: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + KReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
38: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + Slab + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
39: DirectMap1G = Inactive + Active(file) + Active(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
40: DirectMap1G = Inactive + Active(file) + Active(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
41: DirectMap1G = Active + Active(file) + Inactive(anon) + KReclaimable + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
42: DirectMap1G = Active + Active(file) + Inactive(anon) + Slab + SReclaimable + SwapCached + MemFree + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
43: DirectMap1G = Active + Active(file) + Inactive(anon) + Slab + KReclaimable + SwapCached + MemFree + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
44: DirectMap1G = Active + Inactive(file) + Inactive(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
45: DirectMap1G = Active + Inactive(file) + Inactive(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
46: DirectMap1G = Active + Inactive + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
47: DirectMap1G = Active + Inactive + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
48: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
49: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + Slab + SReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
50: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
51: DirectMap1G = Cached + Active(file) + KReclaimable + SReclaimable + MemFree + SUnreclaim + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
52: DirectMap1G = Cached + Active(file) + Slab + SReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
53: DirectMap1G = Cached + Active(file) + Slab + KReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
54: DirectMap1G = Cached + Inactive(file) + SwapCached + VmallocUsed + Shmem + Buffers + KernelStack + Writeback + Hugepagesize
55: DirectMap1G = MemAvailable + AnonPages + SReclaimable + SwapCached + VmallocUsed + Mapped + Mlocked + KernelStack + Writeback + HardwareCorrupted
56: DirectMap1G = MemAvailable + AnonPages + KReclaimable + SwapCached + VmallocUsed + Mapped + Mlocked + KernelStack + Writeback + HardwareCorrupted
57: Committed_AS = AnonHugePages + Active(anon) + SwapTotal + Dirty + DirectMap2M + Slab + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Unevictable + Mlocked + Writeback + Hugepagesize
58: Committed_AS = Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
59: Committed_AS = Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + KReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
60: Committed_AS = AnonPages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
61: Committed_AS = Inactive(file) + SwapTotal + Inactive(anon) + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Unevictable + Mlocked + KernelStack + Hugepagesize
62: Committed_AS = Inactive + Inactive(anon) + Dirty + DirectMap2M + Slab + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Unevictable + Mlocked + HardwareCorrupted
63: Committed_AS = Inactive + SwapTotal + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Unevictable + Mlocked + KernelStack + Hugepagesize
64: Committed_AS = Cached + DirectMap2M + Slab + MemFree + SUnreclaim + DirectMap4k + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
65: Committed_AS = CommitLimit + DirectMap2M + SwapCached + VmallocUsed + Percpu + Mapped + Shmem + Mlocked + Writeback + Hugepagesize
66: CommitLimit = AnonHugePages + Active(anon) + SwapTotal + Dirty + SwapCached + MemFree + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + KernelStack
67: CommitLimit = Active(file) + AnonHugePages + Dirty + DirectMap2M + Slab + SwapCached + MemFree + SUnreclaim + DirectMap4k + PageTables + Mapped + KernelStack + Writeback + Hugepagesize
68: CommitLimit = Inactive(file) + SwapTotal + Inactive(anon) + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
69: CommitLimit = Inactive + SwapTotal + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
70: CommitLimit = Cached + SwapCached + MemFree + VmallocUsed + Mapped + Shmem + Unevictable + Mlocked + Writeback + HardwareCorrupted
71: MemAvailable = AnonPages + AnonHugePages + Dirty + DirectMap2M + MemFree + DirectMap4k + VmallocUsed + Mapped + Unevictable + KernelStack + Writeback + HardwareCorrupted
72: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
73: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
74: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + Slab + SwapCached + MemFree + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
75: MemAvailable = Inactive(file) + Active(file) + PageTables + Percpu + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize
76: MemAvailable = Inactive + SwapTotal + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
77: MemAvailable = Inactive + SwapTotal + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
78: MemAvailable = Inactive + SwapTotal + Slab + SwapCached + MemFree + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
79: Cached = AnonPages + SwapTotal + Inactive(anon) + Slab + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + KernelStack
80: Cached = AnonPages + AnonHugePages + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mlocked + KernelStack + Writeback + HardwareCorrupted
81: Active = Active(anon) + SwapTotal + Inactive(anon) + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mlocked + Writeback + Hugepagesize
82: Active = Active(file) + Active(anon)
83: Inactive = AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SUnreclaim + PageTables + Shmem + Unevictable + KernelStack
84: Inactive = Inactive(file) + Inactive(anon)
85: Inactive(file) = AnonHugePages + Dirty + DirectMap2M + Slab + SUnreclaim + PageTables + Shmem + Unevictable + KernelStack
86: Active(file) = SwapTotal + Inactive(anon) + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mlocked + Writeback + Hugepagesize
87: Slab = SReclaimable + SUnreclaim
88: Slab = KReclaimable + SUnreclaim
89: KReclaimable = SReclaimable
invalid sums (85 sums):
0: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
1: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
2: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
3: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
4: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
5: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
6: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
7: MemTotal = Inactive + Active(file) + Active(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
8: MemTotal = Inactive + Active(file) + Active(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
9: MemTotal = Inactive + Active(file) + Active(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
10: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
11: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
12: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
13: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
14: MemTotal = Active + AnonHugePages + Active(anon) + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
15: MemTotal = Active + AnonHugePages + Active(anon) + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
16: MemTotal = Active + Inactive(file) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
17: MemTotal = Active + Inactive(file) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
18: MemTotal = Active + Inactive(file) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
19: MemTotal = Active + Inactive + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
20: MemTotal = Active + Inactive + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
21: MemTotal = Active + Inactive + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
22: MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
23: MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
24: MemTotal = Cached + SwapTotal + Inactive(anon) + Dirty + SwapCached + SUnreclaim + Percpu + HardwareCorrupted
25: MemTotal = Cached + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
26: MemTotal = Cached + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
27: MemTotal = Cached + Active(anon) + Inactive(anon) + Slab + SwapCached + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
28: MemTotal = MemAvailable + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SReclaimable + SwapCached + Buffers + Hugepagesize
29: MemTotal = CommitLimit + Active(anon) + Inactive(anon) + SUnreclaim + VmallocUsed + Mapped + Shmem + KernelStack + Hugepagesize + HardwareCorrupted
30: DirectMap1G = AnonPages + Active(file) + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + SReclaimable + MemFree + DirectMap4k + VmallocUsed + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
31: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
32: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
33: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + Slab + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
34: DirectMap1G = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
35: DirectMap1G = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
36: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + SReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
37: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + KReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
38: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + Slab + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
39: DirectMap1G = Inactive + Active(file) + Active(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
40: DirectMap1G = Inactive + Active(file) + Active(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
41: DirectMap1G = Active + Active(file) + Inactive(anon) + KReclaimable + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
42: DirectMap1G = Active + Active(file) + Inactive(anon) + Slab + SReclaimable + SwapCached + MemFree + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
43: DirectMap1G = Active + Active(file) + Inactive(anon) + Slab + KReclaimable + SwapCached + MemFree + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
44: DirectMap1G = Active + Inactive(file) + Inactive(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
45: DirectMap1G = Active + Inactive(file) + Inactive(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
46: DirectMap1G = Active + Inactive + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
47: DirectMap1G = Active + Inactive + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
48: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
49: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + Slab + SReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
50: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
51: DirectMap1G = Cached + Active(file) + KReclaimable + SReclaimable + MemFree + SUnreclaim + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
52: DirectMap1G = Cached + Active(file) + Slab + SReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
53: DirectMap1G = Cached + Active(file) + Slab + KReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
54: DirectMap1G = Cached + Inactive(file) + SwapCached + VmallocUsed + Shmem + Buffers + KernelStack + Writeback + Hugepagesize
55: DirectMap1G = MemAvailable + AnonPages + SReclaimable + SwapCached + VmallocUsed + Mapped + Mlocked + KernelStack + Writeback + HardwareCorrupted
56: DirectMap1G = MemAvailable + AnonPages + KReclaimable + SwapCached + VmallocUsed + Mapped + Mlocked + KernelStack + Writeback + HardwareCorrupted
57: Committed_AS = AnonHugePages + Active(anon) + SwapTotal + Dirty + DirectMap2M + Slab + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Unevictable + Mlocked + Writeback + Hugepagesize
58: Committed_AS = Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
59: Committed_AS = Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + KReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
60: Committed_AS = AnonPages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
61: Committed_AS = Inactive(file) + SwapTotal + Inactive(anon) + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Unevictable + Mlocked + KernelStack + Hugepagesize
62: Committed_AS = Inactive + Inactive(anon) + Dirty + DirectMap2M + Slab + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Unevictable + Mlocked + HardwareCorrupted
63: Committed_AS = Inactive + SwapTotal + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Unevictable + Mlocked + KernelStack + Hugepagesize
64: Committed_AS = Cached + DirectMap2M + Slab + MemFree + SUnreclaim + DirectMap4k + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
65: Committed_AS = CommitLimit + DirectMap2M + SwapCached + VmallocUsed + Percpu + Mapped + Shmem + Mlocked + Writeback + Hugepagesize
66: CommitLimit = AnonHugePages + Active(anon) + SwapTotal + Dirty + SwapCached + MemFree + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + KernelStack
67: CommitLimit = Active(file) + AnonHugePages + Dirty + DirectMap2M + Slab + SwapCached + MemFree + SUnreclaim + DirectMap4k + PageTables + Mapped + KernelStack + Writeback + Hugepagesize
68: CommitLimit = Inactive(file) + SwapTotal + Inactive(anon) + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
69: CommitLimit = Inactive + SwapTotal + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
70: CommitLimit = Cached + SwapCached + MemFree + VmallocUsed + Mapped + Shmem + Unevictable + Mlocked + Writeback + HardwareCorrupted
71: MemAvailable = AnonPages + AnonHugePages + Dirty + DirectMap2M + MemFree + DirectMap4k + VmallocUsed + Mapped + Unevictable + KernelStack + Writeback + HardwareCorrupted
72: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
73: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
74: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + Slab + SwapCached + MemFree + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
75: MemAvailable = Inactive(file) + Active(file) + PageTables + Percpu + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize
76: MemAvailable = Inactive + SwapTotal + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
77: MemAvailable = Inactive + SwapTotal + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
78: MemAvailable = Inactive + SwapTotal + Slab + SwapCached + MemFree + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
79: Cached = AnonPages + SwapTotal + Inactive(anon) + Slab + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + KernelStack
80: Cached = AnonPages + AnonHugePages + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mlocked + KernelStack + Writeback + HardwareCorrupted
81: Active = Active(anon) + SwapTotal + Inactive(anon) + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mlocked + Writeback + Hugepagesize
82: Inactive = AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SUnreclaim + PageTables + Shmem + Unevictable + KernelStack
83: Inactive(file) = AnonHugePages + Dirty + DirectMap2M + Slab + SUnreclaim + PageTables + Shmem + Unevictable + KernelStack
84: Active(file) = SwapTotal + Inactive(anon) + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mlocked + Writeback + Hugepagesize
valid sums (5 sums):
0: Active = Active(file) + Active(anon)
1: Inactive = Inactive(file) + Inactive(anon)
2: Slab = SReclaimable + SUnreclaim
3: Slab = KReclaimable + SUnreclaim
4: KReclaimable = SReclaimable
last command status : [0]
```

-> this shows that most equations found by sumfinder on alambix97 set were not confirmed on alambix98 set
