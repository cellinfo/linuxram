find how tmpfs usage appears in /proc/meminfo

```sh
20240813-17:33:29 graffy@graffy-ws2:~/work/linuxram.git$ ./src/test_tmpfs.py 
0
1
2
3
4
0
1
2
3
4
5
5
5
5
effect of the creation of the tmps file /dev/shm/tmpfs-stresser.bin of 200 mib (209715200 bytes):
(decrease) of MemFree (-198955008 bytes: 1775005696 bytes -> 1576050688 bytes)
(decrease) of MemAvailable (-198881280 bytes: 4420767744 bytes -> 4221886464 bytes)
(increase) of Cached (201154560 bytes: 3045748736 bytes -> 3246903296 bytes)
(increase) of Inactive (209649664 bytes: 2264571904 bytes -> 2474221568 bytes)
(increase) of Inactive(anon) (209715200 bytes: 1275138048 bytes -> 1484853248 bytes)
(increase) of Shmem (201293824 bytes: 1080901632 bytes -> 1282195456 bytes)
(increase) of Committed_AS (195084288 bytes: 32823140352 bytes -> 33018224640 bytes)
last command status : [0]
```
