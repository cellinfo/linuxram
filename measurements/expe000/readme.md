This experimentation explores how meminfo's MemAvailable is computed, via an attempt to replicate code used in linux ([file://../../reference/show_mem.c])


```sh
20240813-16:48:45 graffy@graffy-ws2:~/work/linuxram.git$ ./src/memavail.py 
{'MemTotal': 201214205952, 'MemFree': 803061760, 'MemAvailable': 131466833920, 'Buffers': 31981568, 'Cached': 130939863040, 'SwapCached': 1088724992, 'Active': 103304773632, 'Inactive': 94177337344, 'Active(anon)': 39671312384, 'Inactive(anon)': 26918952960, 'Active(file)': 63633461248, 'Inactive(file)': 67258384384, 'Unevictable': 23019520, 'Mlocked': 19873792, 'SwapTotal': 32765898752, 'SwapFree': 0, 'Zswap': 0, 'Zswapped': 0, 'Dirty': 8546779136, 'Writeback': 5550080, 'AnonPages': 65442127872, 'Mapped': 120565760, 'Shmem': 69955584, 'KReclaimable': 1252827136, 'Slab': 1991671808, 'SReclaimable': 1252827136, 'SUnreclaim': 738844672, 'KernelStack': 17088512, 'PageTables': 218902528, 'SecPageTables': 0, 'NFS_Unstable': 0, 'Bounce': 0, 'WritebackTmp': 0, 'CommitLimit': 133373001728, 'Committed_AS': 141631254528, 'VmallocTotal': 35184372087808, 'VmallocUsed': 302333952, 'VmallocChunk': 0, 'Percpu': 215089152, 'HardwareCorrupted': 53248, 'AnonHugePages': 49218060288, 'ShmemHugePages': 0, 'ShmemPmdMapped': 0, 'FileHugePages': 0, 'FilePmdMapped': 0, 'Hugepagesize': 2097152, 'Hugetlb': 0, 'DirectMap4k': 554131456, 'DirectMap2M': 6434062336, 'DirectMap1G': 199715979264}
0: {'DMA': 4, 'DMA32': 529, 'Normal': 34991, 'Movable': 0, 'Device': 0}1: {'DMA': 0, 'DMA32': 0, 'Normal': 36110, 'Movable': 0, 'Device': 0}2: {}
free memory : 784240.0 KiB
cache memory : 127537532.0 KiB
reclaimable memory : 2160392.0 KiB
meminfo's MemAvailable : 128385580.0 KiB
computed available memory : 128691460.0 KiB
computed - meminfo = 305880.0 KiB (76470.0 pages)
last command status : [0]
```
