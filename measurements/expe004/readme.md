# 14/08/2024

as results of expe003 are disappointing (much fewer valid equations than hoped for), this experimentation aims at finding more valid equations by:
- adding new variables (I haven't found the info whether MemTotal includes HardwareCorrupted or not)
	- MemTotalPlusSwapTotal = MemTotal + TotalSwap
	- MemTotalPlusHardwareCorrupted = MemTotal + HardwareCorrupted
	- MemTotalMinusHardwareCorrupted = MemTotal - HardwareCorrupted
	- MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalPlusSwapTotal + HardwareCorrupted
	- MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalPlusSwapTotal - HardwareCorrupted
- merging equations from sumfinder run on multiple meminfo, as some equations might not have been found on alambix97 set because in this case some variables have a value of 0 (and sumfinder ignores them)


## compute the sums for [text](../bug3897/alambix98/20240806-223916-meminfo.stdout)

created ../../src/meminfosums.py 

```sh
20240814-20:46:26 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe004$ ../../src/meminfosums.py --meminfo-stdout-file ../bug3897/alambix98/20240806-223916-meminfo.stdout --id toto
set of integer variables to explore: [MemTotal: 201214218240, DirectMap1G: 198642237440, MemAvailable: 172026621952, MemFree: 138018516992, CommitLimit: 133373005824, Committed_AS: 54422732800, Inactive: 49291243520, Cached: 33713811456, SwapTotal: 32765898752, AnonPages: 26228191232, Inactive(anon): 26218422272, AnonHugePages: 25809649664, Inactive(file): 23072821248, Active: 11497656320, Active(file): 11476578304, DirectMap2M: 7497318400, Slab: 1534443520, KReclaimable: 939606016, Buffers: 863309824, SUnreclaim: 594837504, DirectMap4k: 564633600, VmallocUsed: 304324608, Percpu: 235143168, Mapped: 146780160, PageTables: 67997696, Unevictable: 23019520, Active(anon): 21078016, Mlocked: 19873792, Shmem: 18857984, KernelStack: 17825792, Hugepagesize: 2097152]
sum found: MemTotal = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum found: MemTotal = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum found: MemTotal = Committed_AS + Inactive + SwapTotal + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum found: MemTotal = MemFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum found: MemTotal = MemFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum found: MemTotal = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum found: MemTotal = MemFree + Inactive + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum found: MemTotal = MemFree + Inactive + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Hugepagesize
sum found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + Slab + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Hugepagesize
sum found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active + KReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active + Slab + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum found: DirectMap1G = Committed_AS + Inactive + Cached + AnonPages + Inactive(anon) + DirectMap2M + SUnreclaim + DirectMap4k + PageTables + Active(anon) + Mlocked + Hugepagesize
sum found: DirectMap1G = MemAvailable + Inactive(file) + Slab + KReclaimable + Buffers + Mapped + Mlocked + Shmem + KernelStack + Hugepagesize
sum found: MemAvailable = Inactive + Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + SUnreclaim + DirectMap4k + Percpu + PageTables + Unevictable + Shmem
sum found: MemAvailable = Inactive + Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + Slab + DirectMap4k + Percpu + PageTables + Unevictable + Shmem
sum found: Inactive = Inactive(anon) + Inactive(file)
sum found: Active = Active(file) + Active(anon)
sum found: Slab = KReclaimable + SUnreclaim
exporting sums to toto-sums.json
creating toto-sums.dot from toto-sums.json
creating toto-sums.svg from toto-sums.dot
last command status : [0]
```
# 15/08/2024

```sh
20240815-11:53:16 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe004$ time ../../src/meminfosums.py --meminfo-stdout-file ../bug3897/alambix98/20240806-223916-meminfo.stdout --id alambix98-20240806-223916 --remove-zeros true --remove-duplicate-values=False --max-value 300000000000
dropped variable SwapCached because its value is 0
dropped variable Zswap because its value is 0
dropped variable Zswapped because its value is 0
dropped variable Dirty because its value is 0
dropped variable Writeback because its value is 0
dropped variable SecPageTables because its value is 0
dropped variable NFS_Unstable because its value is 0
dropped variable Bounce because its value is 0
dropped variable WritebackTmp because its value is 0
dropped variable VmallocChunk because its value is 0
dropped variable HardwareCorrupted because its value is 0
dropped variable ShmemHugePages because its value is 0
dropped variable ShmemPmdMapped because its value is 0
dropped variable FileHugePages because its value is 0
dropped variable FilePmdMapped because its value is 0
dropped variable Hugetlb because its value is 0
dropped variable VmallocTotal because is value 35184372087808 exceeds 300000000000
set of integer variables to explore: [MemTotalPlusSwapTotal: 233980116992, MemTotal: 201214218240, DirectMap1G: 198642237440, MemAvailable: 172026621952, MemTotalMinusSwapTotal: 168448319488, MemFree: 138018516992, CommitLimit: 133373005824, Committed_AS: 54422732800, Inactive: 49291243520, Cached: 33713811456, SwapTotal: 32765898752, SwapFree: 32765898752, AnonPages: 26228191232, Inactive(anon): 26218422272, AnonHugePages: 25809649664, Inactive(file): 23072821248, Active: 11497656320, Active(file): 11476578304, DirectMap2M: 7497318400, Slab: 1534443520, KReclaimable: 939606016, SReclaimable: 939606016, Buffers: 863309824, SUnreclaim: 594837504, DirectMap4k: 564633600, VmallocUsed: 304324608, Percpu: 235143168, Mapped: 146780160, PageTables: 67997696, Unevictable: 23019520, Active(anon): 21078016, Mlocked: 19873792, Shmem: 18857984, KernelStack: 17825792, Hugepagesize: 2097152]
sum 0 found: MemTotalPlusSwapTotal = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 1 found: MemTotalPlusSwapTotal = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 2 found: MemTotalPlusSwapTotal = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 3 found: MemTotalPlusSwapTotal = Committed_AS + Inactive + SwapTotal + SwapFree + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 4 found: MemTotalPlusSwapTotal = CommitLimit + Committed_AS + AnonHugePages + Active + DirectMap2M + DirectMap4k + VmallocUsed + Percpu + Mapped + PageTables + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 5 found: MemTotalPlusSwapTotal = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + SReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 6 found: MemTotalPlusSwapTotal = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + KReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 7 found: MemTotalPlusSwapTotal = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active + DirectMap2M + SReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 8 found: MemTotalPlusSwapTotal = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active + DirectMap2M + KReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 9 found: MemTotalPlusSwapTotal = MemFree + SwapFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 10 found: MemTotalPlusSwapTotal = MemFree + SwapFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 11 found: MemTotalPlusSwapTotal = MemFree + SwapFree + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 12 found: MemTotalPlusSwapTotal = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 13 found: MemTotalPlusSwapTotal = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 14 found: MemTotalPlusSwapTotal = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 15 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 16 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 17 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 18 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 19 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 20 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 21 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonHugePages + Inactive(file) + Active(file) + SReclaimable + Buffers + PageTables + KernelStack
sum 22 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + Buffers + PageTables + KernelStack
sum 23 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + SReclaimable + Buffers + SUnreclaim + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 24 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + Buffers + SUnreclaim + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 25 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + Slab + Buffers + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 26 found: MemTotalPlusSwapTotal = MemFree + Inactive + AnonHugePages + Active(file) + DirectMap2M + SReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 27 found: MemTotalPlusSwapTotal = MemFree + Inactive + AnonHugePages + Active(file) + DirectMap2M + KReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 28 found: MemTotalPlusSwapTotal = MemFree + Inactive + AnonHugePages + Active + DirectMap2M + SReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 29 found: MemTotalPlusSwapTotal = MemFree + Inactive + AnonHugePages + Active + DirectMap2M + KReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 30 found: MemTotalPlusSwapTotal = MemFree + Inactive + SwapFree + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 31 found: MemTotalPlusSwapTotal = MemFree + Inactive + SwapFree + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 32 found: MemTotalPlusSwapTotal = MemFree + Inactive + SwapTotal + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 33 found: MemTotalPlusSwapTotal = MemFree + Inactive + SwapTotal + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 34 found: MemTotalPlusSwapTotal = MemTotalMinusSwapTotal + SwapTotal + SwapFree
sum 35 found: MemTotalPlusSwapTotal = MemAvailable + AnonHugePages + Active + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + Buffers + SUnreclaim + DirectMap4k + Mapped + PageTables + Active(anon)
sum 36 found: MemTotalPlusSwapTotal = MemTotal + SwapFree
sum 37 found: MemTotalPlusSwapTotal = MemTotal + SwapTotal
sum 38 found: MemTotal = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 39 found: MemTotal = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 40 found: MemTotal = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 41 found: MemTotal = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 42 found: MemTotal = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 43 found: MemTotal = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 44 found: MemTotal = Committed_AS + Inactive + SwapFree + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 45 found: MemTotal = Committed_AS + Inactive + SwapTotal + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 46 found: MemTotal = MemFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 47 found: MemTotal = MemFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 48 found: MemTotal = MemFree + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 49 found: MemTotal = MemFree + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 50 found: MemTotal = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 51 found: MemTotal = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 52 found: MemTotal = MemFree + Inactive + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 53 found: MemTotal = MemFree + Inactive + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 54 found: MemTotal = MemTotalMinusSwapTotal + SwapFree
sum 55 found: MemTotal = MemTotalMinusSwapTotal + SwapTotal
sum 56 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Hugepagesize
sum 57 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Hugepagesize
sum 58 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + Slab + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Hugepagesize
sum 59 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum 60 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active + KReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum 61 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active + Slab + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum 62 found: DirectMap1G = Committed_AS + Inactive + Cached + AnonPages + Inactive(anon) + DirectMap2M + SUnreclaim + DirectMap4k + PageTables + Active(anon) + Mlocked + Hugepagesize
sum 63 found: DirectMap1G = MemTotalMinusSwapTotal + Inactive(anon) + KReclaimable + SReclaimable + Buffers + DirectMap4k + VmallocUsed + Percpu + PageTables + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 64 found: DirectMap1G = MemAvailable + Inactive(file) + KReclaimable + SReclaimable + Buffers + SUnreclaim + Mapped + Mlocked + Shmem + KernelStack + Hugepagesize
sum 65 found: DirectMap1G = MemAvailable + Inactive(file) + Slab + SReclaimable + Buffers + Mapped + Mlocked + Shmem + KernelStack + Hugepagesize
sum 66 found: DirectMap1G = MemAvailable + Inactive(file) + Slab + KReclaimable + Buffers + Mapped + Mlocked + Shmem + KernelStack + Hugepagesize
sum 67 found: MemAvailable = Inactive + Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + SReclaimable + SUnreclaim + DirectMap4k + Percpu + PageTables + Unevictable + Shmem
sum 68 found: MemAvailable = Inactive + Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + SUnreclaim + DirectMap4k + Percpu + PageTables + Unevictable + Shmem
sum 69 found: MemAvailable = Inactive + Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + Slab + DirectMap4k + Percpu + PageTables + Unevictable + Shmem
sum 70 found: MemAvailable = MemTotalMinusSwapTotal + Slab + Buffers + SUnreclaim + DirectMap4k + Active(anon)
sum 71 found: MemTotalMinusSwapTotal = Inactive + Cached + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 72 found: MemTotalMinusSwapTotal = Inactive + Cached + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 73 found: MemTotalMinusSwapTotal = Inactive + Cached + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 74 found: MemTotalMinusSwapTotal = Committed_AS + Inactive + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 75 found: MemFree = SwapTotal + SwapFree + Inactive(anon) + AnonHugePages + Active(file) + DirectMap2M + SUnreclaim + DirectMap4k + VmallocUsed + Shmem + Hugepagesize
sum 76 found: MemFree = Committed_AS + Cached + Inactive(anon) + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum 77 found: CommitLimit = Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + Buffers + DirectMap4k + VmallocUsed + Percpu + Mapped + Mlocked + Shmem + KernelStack
sum 78 found: CommitLimit = Inactive + Cached + AnonHugePages + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + Buffers + DirectMap4k + VmallocUsed + Percpu + Mapped + Mlocked + Shmem + KernelStack
sum 79 found: CommitLimit = Committed_AS + AnonPages + Inactive(anon) + Active + Active(file) + KReclaimable + SReclaimable + Buffers + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Mlocked + Shmem + KernelStack
sum 80 found: Inactive = Inactive(anon) + Inactive(file)
sum 81 found: Inactive = Cached + Active(file) + Slab + KReclaimable + SReclaimable + VmallocUsed + Percpu + PageTables + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 82 found: Inactive = Cached + Active + Slab + KReclaimable + SReclaimable + VmallocUsed + Percpu + PageTables + Mlocked + Shmem + KernelStack + Hugepagesize
sum 83 found: SwapTotal = SwapFree
sum 84 found: Active = Active(file) + Active(anon)
sum 85 found: Slab = SReclaimable + SUnreclaim
sum 86 found: Slab = KReclaimable + SUnreclaim
sum 87 found: KReclaimable = SReclaimable
exporting sums to alambix98-20240806-223916-sums.json
creating alambix98-20240806-223916-sums.dot from alambix98-20240806-223916-sums.json
creating alambix98-20240806-223916-sums.svg from alambix98-20240806-223916-sums.dot

real    13m54,929s
user    13m54,655s
sys     0m0,116s
last command status : [0]
```

```sh
20240815-12:11:20 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe004$ time ../../src/meminfosums.py --meminfo-stdout-file ../bug3897/alambix97/20240805-191606-meminfo.stdout --id alambix97-20240805-191606 --remove-zeros true --remove-duplicate-values=False --max-value 300000000000
dropped variable SwapFree because its value is 0
dropped variable Zswap because its value is 0
dropped variable Zswapped because its value is 0
dropped variable SecPageTables because its value is 0
dropped variable NFS_Unstable because its value is 0
dropped variable Bounce because its value is 0
dropped variable WritebackTmp because its value is 0
dropped variable VmallocChunk because its value is 0
dropped variable ShmemHugePages because its value is 0
dropped variable ShmemPmdMapped because its value is 0
dropped variable FileHugePages because its value is 0
dropped variable FilePmdMapped because its value is 0
dropped variable Hugetlb because its value is 0
dropped variable VmallocTotal because is value 35184372087808 exceeds 300000000000
set of integer variables to explore: [MemTotalPlusSwapTotalPlusHardwareCorrupted: 233980157952, MemTotalPlusSwapTotal: 233980104704, MemTotalPlusSwapTotalMinusHardwareCorrupted: 233980051456, MemTotalPlusHardwareCorrupted: 201214259200, MemTotal: 201214205952, MemTotalMinusHardwareCorrupted: 201214152704, DirectMap1G: 199715979264, MemTotalMinusSwapTotal: 168448307200, Committed_AS: 141631254528, CommitLimit: 133373001728, MemAvailable: 131466833920, Cached: 130939863040, Active: 103304773632, Inactive: 94177337344, Inactive(file): 67258384384, AnonPages: 65442127872, Active(file): 63633461248, AnonHugePages: 49218060288, Active(anon): 39671312384, SwapTotal: 32765898752, Inactive(anon): 26918952960, Dirty: 8546779136, DirectMap2M: 6434062336, Slab: 1991671808, KReclaimable: 1252827136, SReclaimable: 1252827136, SwapCached: 1088724992, MemFree: 803061760, SUnreclaim: 738844672, DirectMap4k: 554131456, VmallocUsed: 302333952, PageTables: 218902528, Percpu: 215089152, Mapped: 120565760, Shmem: 69955584, Buffers: 31981568, Unevictable: 23019520, Mlocked: 19873792, KernelStack: 17088512, Writeback: 5550080, Hugepagesize: 2097152, HardwareCorrupted: 53248]
sum 0 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 1 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 2 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive(file) + Active(file) + AnonHugePages + Active(anon) + Dirty + KReclaimable + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Mapped + Shmem + Unevictable
sum 3 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive(file) + Active(file) + AnonHugePages + Active(anon) + Dirty + Slab + SReclaimable + SwapCached + MemFree + VmallocUsed + Mapped + Shmem + Unevictable
sum 4 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive(file) + Active(file) + AnonHugePages + Active(anon) + Dirty + Slab + KReclaimable + SwapCached + MemFree + VmallocUsed + Mapped + Shmem + Unevictable
sum 5 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive(file) + AnonPages + Active(file) + Inactive(anon) + Dirty + MemFree + SUnreclaim + VmallocUsed + Percpu + Shmem + Buffers + KernelStack + Hugepagesize
sum 6 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive(file) + AnonPages + Active(file) + SwapTotal + Slab + SwapCached + MemFree + SUnreclaim + Percpu + Unevictable + Mlocked
sum 7 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive(file) + AnonPages + Active(file) + SwapTotal + Slab + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Buffers + Mlocked + Hugepagesize
sum 8 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive(file) + AnonPages + Active(file) + SwapTotal + Slab + KReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Buffers + Mlocked + Hugepagesize
sum 9 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + AnonPages + SwapTotal + Inactive(anon) + Dirty + Slab + KReclaimable + SReclaimable + MemFree + SUnreclaim + Shmem + Mlocked
sum 10 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + AnonPages + Active(file) + Dirty + MemFree + SUnreclaim + VmallocUsed + Percpu + Shmem + Buffers + KernelStack + Hugepagesize
sum 11 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Inactive(file) + SwapTotal + Inactive(anon) + Dirty + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 12 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Inactive(file) + SwapTotal + Inactive(anon) + Dirty + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 13 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Inactive(file) + Active(anon) + Inactive(anon) + Slab + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + PageTables + Shmem + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 14 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Inactive(file) + Active(anon) + Inactive(anon) + Slab + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + PageTables + Shmem + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 15 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Inactive(file) + Active(file) + DirectMap2M + MemFree + SUnreclaim + DirectMap4k + PageTables + Shmem + Buffers + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 16 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Inactive(file) + Active(file) + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + Unevictable + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 17 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Inactive(file) + Active(file) + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + Unevictable + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 18 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Active + AnonHugePages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 19 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Active + AnonHugePages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 20 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Active + Inactive(file) + AnonHugePages + Dirty + KReclaimable + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Mapped + Shmem + Unevictable
sum 21 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Active + Inactive(file) + AnonHugePages + Dirty + Slab + SReclaimable + SwapCached + MemFree + VmallocUsed + Mapped + Shmem + Unevictable
sum 22 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Active + Inactive(file) + AnonHugePages + Dirty + Slab + KReclaimable + SwapCached + MemFree + VmallocUsed + Mapped + Shmem + Unevictable
sum 23 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + Active(file) + Inactive(anon) + DirectMap2M + Slab + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Mapped + Buffers + Unevictable + Mlocked + KernelStack
sum 24 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + Active(file) + Inactive(anon) + DirectMap2M + Slab + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Mapped + Buffers + Unevictable + Mlocked + KernelStack
sum 25 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + Active(file) + Inactive(anon) + Dirty + KReclaimable + SReclaimable + SUnreclaim + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + KernelStack
sum 26 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + Active(file) + Inactive(anon) + Dirty + Slab + SReclaimable + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + KernelStack
sum 27 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + Active(file) + Inactive(anon) + Dirty + Slab + KReclaimable + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + KernelStack
sum 28 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + AnonPages + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + VmallocUsed + PageTables + Unevictable + Hugepagesize + HardwareCorrupted
sum 29 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + AnonPages + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + VmallocUsed + PageTables + Unevictable + Hugepagesize + HardwareCorrupted
sum 30 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + AnonPages + Inactive(anon) + DirectMap2M + KReclaimable + SReclaimable + MemFree + SUnreclaim + Mapped + Buffers + Unevictable + Mlocked + Hugepagesize + HardwareCorrupted
sum 31 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + AnonPages + Inactive(anon) + DirectMap2M + Slab + SReclaimable + MemFree + Mapped + Buffers + Unevictable + Mlocked + Hugepagesize + HardwareCorrupted
sum 32 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Cached + AnonPages + Inactive(anon) + DirectMap2M + Slab + KReclaimable + MemFree + Mapped + Buffers + Unevictable + Mlocked + Hugepagesize + HardwareCorrupted
sum 33 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = CommitLimit + AnonHugePages + Active(anon) + Dirty + SwapCached + MemFree + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + KernelStack + HardwareCorrupted
sum 34 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = CommitLimit + Active(file) + SwapTotal + Slab + SReclaimable + SUnreclaim + PageTables + Writeback
sum 35 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = CommitLimit + Active(file) + SwapTotal + Slab + KReclaimable + SUnreclaim + PageTables + Writeback
sum 36 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = CommitLimit + Inactive(file) + Inactive(anon) + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 37 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = CommitLimit + Inactive + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 38 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotal + SwapTotal + HardwareCorrupted
sum 39 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalPlusHardwareCorrupted + SwapTotal
sum 40 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalPlusSwapTotal + HardwareCorrupted
sum 41 found: MemTotalPlusSwapTotal = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 42 found: MemTotalPlusSwapTotal = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum 43 found: MemTotalPlusSwapTotal = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 44 found: MemTotalPlusSwapTotal = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum 45 found: MemTotalPlusSwapTotal = Inactive(file) + Active(file) + Active(anon) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 46 found: MemTotalPlusSwapTotal = Inactive(file) + Active(file) + Active(anon) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 47 found: MemTotalPlusSwapTotal = Inactive(file) + Active(file) + Active(anon) + SwapTotal + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 48 found: MemTotalPlusSwapTotal = Inactive(file) + AnonPages + AnonHugePages + Active(anon) + DirectMap2M + Slab + KReclaimable + SReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 49 found: MemTotalPlusSwapTotal = Inactive + Active(file) + Active(anon) + Inactive(anon) + DirectMap2M + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 50 found: MemTotalPlusSwapTotal = Inactive + Active(file) + Active(anon) + SwapTotal + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 51 found: MemTotalPlusSwapTotal = Inactive + Active(file) + Active(anon) + SwapTotal + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 52 found: MemTotalPlusSwapTotal = Inactive + Active(file) + Active(anon) + SwapTotal + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 53 found: MemTotalPlusSwapTotal = Inactive + Inactive(file) + Active(anon) + Inactive(anon) + Slab + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + PageTables + Shmem + Mlocked + KernelStack + Hugepagesize
sum 54 found: MemTotalPlusSwapTotal = Inactive + Inactive(file) + Active(anon) + Inactive(anon) + Slab + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + PageTables + Shmem + Mlocked + KernelStack + Hugepagesize
sum 55 found: MemTotalPlusSwapTotal = Inactive + Inactive(file) + Active(file) + DirectMap2M + MemFree + SUnreclaim + DirectMap4k + PageTables + Shmem + Buffers + Unevictable + Mlocked + KernelStack
sum 56 found: MemTotalPlusSwapTotal = Inactive + Inactive(file) + Active(file) + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + Unevictable + Mlocked + Writeback + Hugepagesize
sum 57 found: MemTotalPlusSwapTotal = Inactive + Inactive(file) + Active(file) + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + Unevictable + Mlocked + Writeback + Hugepagesize
sum 58 found: MemTotalPlusSwapTotal = Active + AnonHugePages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 59 found: MemTotalPlusSwapTotal = Active + AnonHugePages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum 60 found: MemTotalPlusSwapTotal = Active + AnonHugePages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 61 found: MemTotalPlusSwapTotal = Active + AnonHugePages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum 62 found: MemTotalPlusSwapTotal = Active + AnonHugePages + Active(anon) + SwapTotal + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 63 found: MemTotalPlusSwapTotal = Active + AnonHugePages + Active(anon) + SwapTotal + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 64 found: MemTotalPlusSwapTotal = Active + AnonPages + Active(file) + SwapCached + PageTables + Percpu + Buffers + Unevictable + Mlocked + Hugepagesize + HardwareCorrupted
sum 65 found: MemTotalPlusSwapTotal = Active + Inactive(file) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 66 found: MemTotalPlusSwapTotal = Active + Inactive(file) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 67 found: MemTotalPlusSwapTotal = Active + Inactive(file) + SwapTotal + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 68 found: MemTotalPlusSwapTotal = Active + Inactive + Inactive(anon) + DirectMap2M + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 69 found: MemTotalPlusSwapTotal = Active + Inactive + SwapTotal + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 70 found: MemTotalPlusSwapTotal = Active + Inactive + SwapTotal + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 71 found: MemTotalPlusSwapTotal = Active + Inactive + SwapTotal + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 72 found: MemTotalPlusSwapTotal = Cached + Active(anon) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum 73 found: MemTotalPlusSwapTotal = Cached + Active(anon) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum 74 found: MemTotalPlusSwapTotal = Cached + Active(anon) + SwapTotal + Inactive(anon) + Slab + SwapCached + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum 75 found: MemTotalPlusSwapTotal = Cached + AnonHugePages + Active(anon) + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + VmallocUsed + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 76 found: MemTotalPlusSwapTotal = Cached + AnonPages + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + VmallocUsed + PageTables + Unevictable + Hugepagesize
sum 77 found: MemTotalPlusSwapTotal = Cached + AnonPages + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + VmallocUsed + PageTables + Unevictable + Hugepagesize
sum 78 found: MemTotalPlusSwapTotal = Cached + AnonPages + Inactive(anon) + DirectMap2M + KReclaimable + SReclaimable + MemFree + SUnreclaim + Mapped + Buffers + Unevictable + Mlocked + Hugepagesize
sum 79 found: MemTotalPlusSwapTotal = Cached + AnonPages + Inactive(anon) + DirectMap2M + Slab + SReclaimable + MemFree + Mapped + Buffers + Unevictable + Mlocked + Hugepagesize
sum 80 found: MemTotalPlusSwapTotal = Cached + AnonPages + Inactive(anon) + DirectMap2M + Slab + KReclaimable + MemFree + Mapped + Buffers + Unevictable + Mlocked + Hugepagesize
sum 81 found: MemTotalPlusSwapTotal = CommitLimit + Active(anon) + SwapTotal + Inactive(anon) + SUnreclaim + VmallocUsed + Mapped + Shmem + KernelStack + Hugepagesize + HardwareCorrupted
sum 82 found: MemTotalPlusSwapTotal = CommitLimit + AnonHugePages + Active(anon) + Dirty + SwapCached + MemFree + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + KernelStack
sum 83 found: MemTotalPlusSwapTotal = CommitLimit + Inactive(file) + Inactive(anon) + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
sum 84 found: MemTotalPlusSwapTotal = CommitLimit + Inactive + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
sum 85 found: MemTotalPlusSwapTotal = MemTotalMinusSwapTotal + AnonHugePages + Dirty + Slab + KReclaimable + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Shmem + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 86 found: MemTotalPlusSwapTotal = MemTotalMinusHardwareCorrupted + SwapTotal + HardwareCorrupted
sum 87 found: MemTotalPlusSwapTotal = MemTotal + SwapTotal
sum 88 found: MemTotalPlusSwapTotal = MemTotalPlusHardwareCorrupted + Inactive(anon) + Slab + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback + HardwareCorrupted
sum 89 found: MemTotalPlusSwapTotal = MemTotalPlusHardwareCorrupted + Inactive(anon) + Slab + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback + HardwareCorrupted
sum 90 found: MemTotalPlusSwapTotal = MemTotalPlusSwapTotalMinusHardwareCorrupted + HardwareCorrupted
sum 91 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 92 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 93 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive(file) + Active(file) + Active(anon) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 94 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive(file) + Active(file) + Active(anon) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 95 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive(file) + Active(file) + Active(anon) + SwapTotal + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 96 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive(file) + AnonPages + AnonHugePages + Active(anon) + DirectMap2M + Slab + KReclaimable + SReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize
sum 97 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive + Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Buffers + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 98 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive + Active(file) + Active(anon) + Inactive(anon) + DirectMap2M + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Writeback
sum 99 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive + Active(file) + Active(anon) + SwapTotal + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 100 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive + Active(file) + Active(anon) + SwapTotal + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 101 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive + Active(file) + Active(anon) + SwapTotal + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 102 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + AnonHugePages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 103 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + AnonHugePages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 104 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Mapped + Shmem + Mlocked + KernelStack + HardwareCorrupted
sum 105 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + AnonHugePages + Active(anon) + SwapTotal + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize
sum 106 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + AnonHugePages + Active(anon) + SwapTotal + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize
sum 107 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + AnonPages + Active(file) + SwapCached + PageTables + Percpu + Buffers + Unevictable + Mlocked + Hugepagesize
sum 108 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + Inactive(file) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 109 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + Inactive(file) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 110 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + Inactive(file) + SwapTotal + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 111 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + Inactive + Inactive(anon) + DirectMap2M + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Writeback
sum 112 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + Inactive + SwapTotal + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 113 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + Inactive + SwapTotal + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 114 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Active + Inactive + SwapTotal + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 115 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Cached + Active(anon) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack
sum 116 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Cached + Active(anon) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack
sum 117 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Cached + Active(anon) + SwapTotal + Inactive(anon) + Slab + SwapCached + PageTables + Percpu + Mapped + Buffers + KernelStack
sum 118 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Cached + AnonHugePages + Active(anon) + Dirty + Slab + SReclaimable + SwapCached + SUnreclaim + VmallocUsed + Mapped + Shmem + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 119 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Cached + AnonHugePages + Active(anon) + Dirty + Slab + KReclaimable + SwapCached + SUnreclaim + VmallocUsed + Mapped + Shmem + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 120 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Cached + AnonHugePages + Active(anon) + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + VmallocUsed + Unevictable + Mlocked + KernelStack + Writeback
sum 121 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Cached + Active(file) + Inactive(anon) + Dirty + KReclaimable + SReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Shmem + Buffers + Unevictable + Mlocked + HardwareCorrupted
sum 122 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Cached + AnonPages + Inactive(anon) + Dirty + MemFree + SUnreclaim + VmallocUsed + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 123 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemAvailable + AnonHugePages + SwapTotal + Dirty + DirectMap2M + KReclaimable + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + HardwareCorrupted
sum 124 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemAvailable + AnonHugePages + SwapTotal + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + HardwareCorrupted
sum 125 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemAvailable + AnonHugePages + SwapTotal + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + HardwareCorrupted
sum 126 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = CommitLimit + Active(anon) + SwapTotal + Inactive(anon) + SUnreclaim + VmallocUsed + Mapped + Shmem + KernelStack + Hugepagesize
sum 127 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + SwapTotal + Dirty + DirectMap2M + SReclaimable + SwapCached + SUnreclaim + VmallocUsed + Mapped + Shmem + Buffers + Mlocked + KernelStack + HardwareCorrupted
sum 128 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + SwapTotal + Dirty + DirectMap2M + KReclaimable + SwapCached + SUnreclaim + VmallocUsed + Mapped + Shmem + Buffers + Mlocked + KernelStack + HardwareCorrupted
sum 129 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + SwapTotal + Dirty + DirectMap2M + Slab + SwapCached + VmallocUsed + Mapped + Shmem + Buffers + Mlocked + KernelStack + HardwareCorrupted
sum 130 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + Active(anon) + DirectMap2M + KReclaimable + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Mapped + Buffers + Unevictable + Writeback + HardwareCorrupted
sum 131 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + Active(anon) + DirectMap2M + Slab + SReclaimable + SwapCached + DirectMap4k + Percpu + Mapped + Buffers + Unevictable + Writeback + HardwareCorrupted
sum 132 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + Active(anon) + DirectMap2M + Slab + KReclaimable + SwapCached + DirectMap4k + Percpu + Mapped + Buffers + Unevictable + Writeback + HardwareCorrupted
sum 133 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Committed_AS + AnonHugePages + SwapTotal + Dirty + SReclaimable + VmallocUsed + Percpu + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 134 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Committed_AS + AnonHugePages + SwapTotal + Dirty + KReclaimable + VmallocUsed + Percpu + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 135 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalMinusSwapTotal + SwapTotal + Inactive(anon) + Slab + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback + HardwareCorrupted
sum 136 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalMinusSwapTotal + SwapTotal + Inactive(anon) + Slab + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback + HardwareCorrupted
sum 137 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalMinusSwapTotal + AnonHugePages + Dirty + Slab + KReclaimable + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Shmem + Unevictable + KernelStack + Writeback + Hugepagesize
sum 138 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalMinusHardwareCorrupted + SwapTotal
sum 139 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotal + Inactive(anon) + Slab + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback + HardwareCorrupted
sum 140 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotal + Inactive(anon) + Slab + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback + HardwareCorrupted
sum 141 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalPlusHardwareCorrupted + Inactive(anon) + Slab + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback
sum 142 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalPlusHardwareCorrupted + Inactive(anon) + Slab + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback
sum 143 found: MemTotalPlusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 144 found: MemTotalPlusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 145 found: MemTotalPlusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Dirty + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Shmem + KernelStack + Writeback
sum 146 found: MemTotalPlusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + SwapTotal + Dirty + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Shmem + KernelStack + Writeback
sum 147 found: MemTotalPlusHardwareCorrupted = Inactive(file) + AnonPages + Active(file) + Slab + SwapCached + MemFree + SUnreclaim + Percpu + Unevictable + Mlocked
sum 148 found: MemTotalPlusHardwareCorrupted = Inactive(file) + AnonPages + Active(file) + Slab + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Buffers + Mlocked + Hugepagesize
sum 149 found: MemTotalPlusHardwareCorrupted = Inactive(file) + AnonPages + Active(file) + Slab + KReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Buffers + Mlocked + Hugepagesize
sum 150 found: MemTotalPlusHardwareCorrupted = Inactive + AnonPages + Inactive(anon) + Dirty + Slab + KReclaimable + SReclaimable + MemFree + SUnreclaim + Shmem + Mlocked
sum 151 found: MemTotalPlusHardwareCorrupted = Inactive + Inactive(file) + Inactive(anon) + Dirty + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 152 found: MemTotalPlusHardwareCorrupted = Inactive + Inactive(file) + Inactive(anon) + Dirty + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 153 found: MemTotalPlusHardwareCorrupted = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 154 found: MemTotalPlusHardwareCorrupted = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack + HardwareCorrupted
sum 155 found: MemTotalPlusHardwareCorrupted = Active + AnonHugePages + SwapTotal + Dirty + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Shmem + KernelStack + Writeback
sum 156 found: MemTotalPlusHardwareCorrupted = Active + AnonHugePages + SwapTotal + Dirty + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Shmem + KernelStack + Writeback
sum 157 found: MemTotalPlusHardwareCorrupted = MemAvailable + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SReclaimable + SwapCached + Buffers + Hugepagesize + HardwareCorrupted
sum 158 found: MemTotalPlusHardwareCorrupted = CommitLimit + Active(file) + Slab + SReclaimable + SUnreclaim + PageTables + Writeback
sum 159 found: MemTotalPlusHardwareCorrupted = CommitLimit + Active(file) + Slab + KReclaimable + SUnreclaim + PageTables + Writeback
sum 160 found: MemTotalPlusHardwareCorrupted = MemTotalMinusSwapTotal + SwapTotal + HardwareCorrupted
sum 161 found: MemTotalPlusHardwareCorrupted = MemTotal + HardwareCorrupted
sum 162 found: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 163 found: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum 164 found: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 165 found: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum 166 found: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 167 found: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 168 found: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 169 found: MemTotal = Inactive + Active(file) + Active(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 170 found: MemTotal = Inactive + Active(file) + Active(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 171 found: MemTotal = Inactive + Active(file) + Active(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 172 found: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 173 found: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum 174 found: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 175 found: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum 176 found: MemTotal = Active + AnonHugePages + Active(anon) + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 177 found: MemTotal = Active + AnonHugePages + Active(anon) + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 178 found: MemTotal = Active + Inactive(file) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 179 found: MemTotal = Active + Inactive(file) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 180 found: MemTotal = Active + Inactive(file) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 181 found: MemTotal = Active + Inactive + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 182 found: MemTotal = Active + Inactive + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 183 found: MemTotal = Active + Inactive + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 184 found: MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
sum 185 found: MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
sum 186 found: MemTotal = Cached + SwapTotal + Inactive(anon) + Dirty + SwapCached + SUnreclaim + Percpu + HardwareCorrupted
sum 187 found: MemTotal = Cached + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum 188 found: MemTotal = Cached + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum 189 found: MemTotal = Cached + Active(anon) + Inactive(anon) + Slab + SwapCached + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum 190 found: MemTotal = MemAvailable + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SReclaimable + SwapCached + Buffers + Hugepagesize
sum 191 found: MemTotal = CommitLimit + Active(anon) + Inactive(anon) + SUnreclaim + VmallocUsed + Mapped + Shmem + KernelStack + Hugepagesize + HardwareCorrupted
sum 192 found: MemTotal = MemTotalMinusSwapTotal + SwapTotal
sum 193 found: MemTotal = MemTotalMinusHardwareCorrupted + HardwareCorrupted
sum 194 found: MemTotalMinusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 195 found: MemTotalMinusHardwareCorrupted = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 196 found: MemTotalMinusHardwareCorrupted = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + Slab + SwapCached + SUnreclaim + Percpu + Mapped + Unevictable + Mlocked + Writeback + HardwareCorrupted
sum 197 found: MemTotalMinusHardwareCorrupted = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 198 found: MemTotalMinusHardwareCorrupted = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 199 found: MemTotalMinusHardwareCorrupted = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 200 found: MemTotalMinusHardwareCorrupted = Inactive + Active(file) + Inactive(anon) + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Buffers + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 201 found: MemTotalMinusHardwareCorrupted = Inactive + Active(file) + SwapTotal + DirectMap2M + Slab + SwapCached + SUnreclaim + Percpu + Mapped + Unevictable + Mlocked + Writeback + HardwareCorrupted
sum 202 found: MemTotalMinusHardwareCorrupted = Inactive + Active(file) + Active(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 203 found: MemTotalMinusHardwareCorrupted = Inactive + Active(file) + Active(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 204 found: MemTotalMinusHardwareCorrupted = Inactive + Active(file) + Active(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 205 found: MemTotalMinusHardwareCorrupted = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 206 found: MemTotalMinusHardwareCorrupted = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize
sum 207 found: MemTotalMinusHardwareCorrupted = Active + AnonHugePages + Active(anon) + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize
sum 208 found: MemTotalMinusHardwareCorrupted = Active + AnonHugePages + Active(anon) + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize
sum 209 found: MemTotalMinusHardwareCorrupted = Active + Inactive(file) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 210 found: MemTotalMinusHardwareCorrupted = Active + Inactive(file) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 211 found: MemTotalMinusHardwareCorrupted = Active + Inactive(file) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 212 found: MemTotalMinusHardwareCorrupted = Active + Inactive + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 213 found: MemTotalMinusHardwareCorrupted = Active + Inactive + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 214 found: MemTotalMinusHardwareCorrupted = Active + Inactive + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback
sum 215 found: MemTotalMinusHardwareCorrupted = Cached + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback
sum 216 found: MemTotalMinusHardwareCorrupted = Cached + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback
sum 217 found: MemTotalMinusHardwareCorrupted = Cached + SwapTotal + Inactive(anon) + Dirty + SwapCached + SUnreclaim + Percpu
sum 218 found: MemTotalMinusHardwareCorrupted = Cached + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack
sum 219 found: MemTotalMinusHardwareCorrupted = Cached + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack
sum 220 found: MemTotalMinusHardwareCorrupted = Cached + Active(anon) + Inactive(anon) + Slab + SwapCached + PageTables + Percpu + Mapped + Buffers + KernelStack
sum 221 found: MemTotalMinusHardwareCorrupted = MemAvailable + SwapTotal + Inactive(anon) + Dirty + SwapCached + VmallocUsed + Shmem + Buffers + KernelStack + Writeback + HardwareCorrupted
sum 222 found: MemTotalMinusHardwareCorrupted = MemAvailable + AnonHugePages + Dirty + DirectMap2M + KReclaimable + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + HardwareCorrupted
sum 223 found: MemTotalMinusHardwareCorrupted = MemAvailable + AnonHugePages + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + HardwareCorrupted
sum 224 found: MemTotalMinusHardwareCorrupted = MemAvailable + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + HardwareCorrupted
sum 225 found: MemTotalMinusHardwareCorrupted = CommitLimit + Active(anon) + Inactive(anon) + SUnreclaim + VmallocUsed + Mapped + Shmem + KernelStack + Hugepagesize
sum 226 found: MemTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + Dirty + DirectMap2M + SReclaimable + SwapCached + SUnreclaim + VmallocUsed + Mapped + Shmem + Buffers + Mlocked + KernelStack + HardwareCorrupted
sum 227 found: MemTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + Dirty + DirectMap2M + KReclaimable + SwapCached + SUnreclaim + VmallocUsed + Mapped + Shmem + Buffers + Mlocked + KernelStack + HardwareCorrupted
sum 228 found: MemTotalMinusHardwareCorrupted = CommitLimit + AnonHugePages + Dirty + DirectMap2M + Slab + SwapCached + VmallocUsed + Mapped + Shmem + Buffers + Mlocked + KernelStack + HardwareCorrupted
sum 229 found: MemTotalMinusHardwareCorrupted = Committed_AS + AnonHugePages + Dirty + SReclaimable + VmallocUsed + Percpu + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 230 found: MemTotalMinusHardwareCorrupted = Committed_AS + AnonHugePages + Dirty + KReclaimable + VmallocUsed + Percpu + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 231 found: MemTotalMinusHardwareCorrupted = MemTotalMinusSwapTotal + Inactive(anon) + Slab + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback + HardwareCorrupted
sum 232 found: MemTotalMinusHardwareCorrupted = MemTotalMinusSwapTotal + Inactive(anon) + Slab + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Percpu + Writeback + HardwareCorrupted
sum 233 found: DirectMap1G = AnonPages + Active(file) + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + SReclaimable + MemFree + DirectMap4k + VmallocUsed + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum 234 found: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum 235 found: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum 236 found: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + Slab + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum 237 found: DirectMap1G = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum 238 found: DirectMap1G = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum 239 found: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + SReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum 240 found: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + KReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum 241 found: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + Slab + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum 242 found: DirectMap1G = Inactive + Active(file) + Active(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum 243 found: DirectMap1G = Inactive + Active(file) + Active(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum 244 found: DirectMap1G = Active + Active(file) + Inactive(anon) + KReclaimable + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 245 found: DirectMap1G = Active + Active(file) + Inactive(anon) + Slab + SReclaimable + SwapCached + MemFree + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 246 found: DirectMap1G = Active + Active(file) + Inactive(anon) + Slab + KReclaimable + SwapCached + MemFree + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 247 found: DirectMap1G = Active + Inactive(file) + Inactive(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum 248 found: DirectMap1G = Active + Inactive(file) + Inactive(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum 249 found: DirectMap1G = Active + Inactive + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum 250 found: DirectMap1G = Active + Inactive + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum 251 found: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
sum 252 found: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + Slab + SReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
sum 253 found: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
sum 254 found: DirectMap1G = Cached + Active(file) + KReclaimable + SReclaimable + MemFree + SUnreclaim + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
sum 255 found: DirectMap1G = Cached + Active(file) + Slab + SReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
sum 256 found: DirectMap1G = Cached + Active(file) + Slab + KReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
sum 257 found: DirectMap1G = Cached + Inactive(file) + SwapCached + VmallocUsed + Shmem + Buffers + KernelStack + Writeback + Hugepagesize
sum 258 found: DirectMap1G = MemAvailable + AnonPages + SReclaimable + SwapCached + VmallocUsed + Mapped + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 259 found: DirectMap1G = MemAvailable + AnonPages + KReclaimable + SwapCached + VmallocUsed + Mapped + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 260 found: MemTotalMinusSwapTotal = Inactive(file) + Active(file) + SwapTotal + KReclaimable + SReclaimable + SwapCached + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + KernelStack + Writeback + HardwareCorrupted
sum 261 found: MemTotalMinusSwapTotal = Cached + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
sum 262 found: MemTotalMinusSwapTotal = Cached + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
sum 263 found: MemTotalMinusSwapTotal = Cached + Inactive(anon) + Dirty + SwapCached + SUnreclaim + Percpu + HardwareCorrupted
sum 264 found: MemTotalMinusSwapTotal = MemAvailable + Inactive(anon) + DirectMap2M + KReclaimable + SReclaimable + SwapCached + Buffers + Hugepagesize
sum 265 found: MemTotalMinusSwapTotal = MemAvailable + SwapTotal + KReclaimable + SReclaimable + MemFree + DirectMap4k + Percpu + Shmem + Unevictable + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 266 found: MemTotalMinusSwapTotal = MemAvailable + SwapTotal + Slab + SReclaimable + SUnreclaim + Percpu + KernelStack + HardwareCorrupted
sum 267 found: MemTotalMinusSwapTotal = MemAvailable + SwapTotal + Slab + KReclaimable + SUnreclaim + Percpu + KernelStack + HardwareCorrupted
sum 268 found: MemTotalMinusSwapTotal = CommitLimit + SwapTotal + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum 269 found: Committed_AS = AnonHugePages + Active(anon) + SwapTotal + Dirty + DirectMap2M + Slab + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Unevictable + Mlocked + Writeback + Hugepagesize
sum 270 found: Committed_AS = Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 271 found: Committed_AS = Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + KReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 272 found: Committed_AS = AnonPages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 273 found: Committed_AS = Inactive(file) + SwapTotal + Inactive(anon) + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 274 found: Committed_AS = Inactive + Inactive(anon) + Dirty + DirectMap2M + Slab + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Unevictable + Mlocked + HardwareCorrupted
sum 275 found: Committed_AS = Inactive + SwapTotal + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 276 found: Committed_AS = Cached + DirectMap2M + Slab + MemFree + SUnreclaim + DirectMap4k + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum 277 found: Committed_AS = CommitLimit + DirectMap2M + SwapCached + VmallocUsed + Percpu + Mapped + Shmem + Mlocked + Writeback + Hugepagesize
sum 278 found: CommitLimit = AnonHugePages + Active(anon) + SwapTotal + Dirty + SwapCached + MemFree + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + KernelStack
sum 279 found: CommitLimit = Active(file) + AnonHugePages + Dirty + DirectMap2M + Slab + SwapCached + MemFree + SUnreclaim + DirectMap4k + PageTables + Mapped + KernelStack + Writeback + Hugepagesize
sum 280 found: CommitLimit = Inactive(file) + SwapTotal + Inactive(anon) + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
sum 281 found: CommitLimit = Inactive + SwapTotal + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
sum 282 found: CommitLimit = Cached + SwapCached + MemFree + VmallocUsed + Mapped + Shmem + Unevictable + Mlocked + Writeback + HardwareCorrupted
sum 283 found: MemAvailable = AnonPages + AnonHugePages + Dirty + DirectMap2M + MemFree + DirectMap4k + VmallocUsed + Mapped + Unevictable + KernelStack + Writeback + HardwareCorrupted
sum 284 found: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum 285 found: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum 286 found: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + Slab + SwapCached + MemFree + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum 287 found: MemAvailable = Inactive(file) + Active(file) + PageTables + Percpu + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize
sum 288 found: MemAvailable = Inactive + SwapTotal + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum 289 found: MemAvailable = Inactive + SwapTotal + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum 290 found: MemAvailable = Inactive + SwapTotal + Slab + SwapCached + MemFree + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum 291 found: Cached = AnonPages + SwapTotal + Inactive(anon) + Slab + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + KernelStack
sum 292 found: Cached = AnonPages + AnonHugePages + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum 293 found: Active = Active(anon) + SwapTotal + Inactive(anon) + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mlocked + Writeback + Hugepagesize
sum 294 found: Active = Active(file) + Active(anon)
sum 295 found: Inactive = AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SUnreclaim + PageTables + Shmem + Unevictable + KernelStack
sum 296 found: Inactive = Inactive(file) + Inactive(anon)
sum 297 found: Inactive(file) = AnonHugePages + Dirty + DirectMap2M + Slab + SUnreclaim + PageTables + Shmem + Unevictable + KernelStack
sum 298 found: Active(file) = SwapTotal + Inactive(anon) + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mlocked + Writeback + Hugepagesize
sum 299 found: Slab = SReclaimable + SUnreclaim
sum 300 found: Slab = KReclaimable + SUnreclaim
sum 301 found: KReclaimable = SReclaimable
exporting sums to alambix97-20240805-191606-sums.json
creating alambix97-20240805-191606-sums.dot from alambix97-20240805-191606-sums.json
creating alambix97-20240805-191606-sums.svg from alambix97-20240805-191606-sums.dot

real    41m1,532s
user    40m59,934s
sys     0m1,056s
last command status : [0]
```

# 16/08/2024 

- created test_sumfinder to validate Sums.merge_sums on set001 and set002... fixed a bug and now it works!

```sh
20240816-11:36:02 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe004$ ../../src/mergesums.py --input-sums-files alambix97-20240805-191606-sums.json alambix98-20240806-223916-sums.json --output-sums-file mergedmeminfosums.json
[PosixPath('alambix97-20240805-191606-sums.json'), PosixPath('alambix98-20240806-223916-sums.json')]
Traceback (most recent call last):
  File "../../src/mergesums.py", line 23, in <module>
    main()
  File "../../src/mergesums.py", line 19, in main
    merged_sums = Sums.merge_sums(input_sums)
  File "/home/graffy/work/linuxram.git/src/sumfinder/sumfinder.py", line 150, in merge_sums
    valid_sums, invalid_sums = sums.validate_on_variables(other_sums.get_variables_as_dict())
  File "/home/graffy/work/linuxram.git/src/sumfinder/sumfinder.py", line 90, in validate_on_variables
    if Sums.sum_is_valid_with_variables(_sum, variables):
  File "/home/graffy/work/linuxram.git/src/sumfinder/sumfinder.py", line 75, in sum_is_valid_with_variables
    real_total_value = variables[total_var_id]
KeyError: 'MemTotalPlusSwapTotalPlusHardwareCorrupted'
last command status : [0]
```

- recreated [text](alambix98-meminfo-sums.json) because the previous one was generated with buggy code (missing MemTotalPlusSwapTotalPlusHardwareCorrupted when HardwareCorrupted == 0)

```sh
20240816-11:36:12 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe004$ time ../../src/meminfosums.py --meminfo-stdout-file ../bug3897/alambix98/20240806-223916-meminfo.stdout --id alambix98-20240806-223916 --remove-zeros true --remove-duplicate-values=False --max-value 300000000000
dropped variable SwapCached because its value is 0
dropped variable Zswap because its value is 0
dropped variable Zswapped because its value is 0
dropped variable Dirty because its value is 0
dropped variable Writeback because its value is 0
dropped variable SecPageTables because its value is 0
dropped variable NFS_Unstable because its value is 0
dropped variable Bounce because its value is 0
dropped variable WritebackTmp because its value is 0
dropped variable VmallocChunk because its value is 0
dropped variable HardwareCorrupted because its value is 0
dropped variable ShmemHugePages because its value is 0
dropped variable ShmemPmdMapped because its value is 0
dropped variable FileHugePages because its value is 0
dropped variable FilePmdMapped because its value is 0
dropped variable Hugetlb because its value is 0
dropped variable VmallocTotal because is value 35184372087808 exceeds 300000000000
set of integer variables to explore: [MemTotalPlusSwapTotal: 233980116992, MemTotalPlusSwapTotalPlusHardwareCorrupted: 233980116992, MemTotalPlusSwapTotalMinusHardwareCorrupted: 233980116992, MemTotal: 201214218240, MemTotalPlusHardwareCorrupted: 201214218240, MemTotalMinusHardwareCorrupted: 201214218240, DirectMap1G: 198642237440, MemAvailable: 172026621952, MemTotalMinusSwapTotal: 168448319488, MemFree: 138018516992, CommitLimit: 133373005824, Committed_AS: 54422732800, Inactive: 49291243520, Cached: 33713811456, SwapTotal: 32765898752, SwapFree: 32765898752, AnonPages: 26228191232, Inactive(anon): 26218422272, AnonHugePages: 25809649664, Inactive(file): 23072821248, Active: 11497656320, Active(file): 11476578304, DirectMap2M: 7497318400, Slab: 1534443520, KReclaimable: 939606016, SReclaimable: 939606016, Buffers: 863309824, SUnreclaim: 594837504, DirectMap4k: 564633600, VmallocUsed: 304324608, Percpu: 235143168, Mapped: 146780160, PageTables: 67997696, Unevictable: 23019520, Active(anon): 21078016, Mlocked: 19873792, Shmem: 18857984, KernelStack: 17825792, Hugepagesize: 2097152]
sum 0 found: MemTotalPlusSwapTotal = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 1 found: MemTotalPlusSwapTotal = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 2 found: MemTotalPlusSwapTotal = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 3 found: MemTotalPlusSwapTotal = Committed_AS + Inactive + SwapTotal + SwapFree + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 4 found: MemTotalPlusSwapTotal = CommitLimit + Committed_AS + AnonHugePages + Active + DirectMap2M + DirectMap4k + VmallocUsed + Percpu + Mapped + PageTables + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 5 found: MemTotalPlusSwapTotal = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + SReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 6 found: MemTotalPlusSwapTotal = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + KReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 7 found: MemTotalPlusSwapTotal = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active + DirectMap2M + SReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 8 found: MemTotalPlusSwapTotal = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active + DirectMap2M + KReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 9 found: MemTotalPlusSwapTotal = MemFree + SwapFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 10 found: MemTotalPlusSwapTotal = MemFree + SwapFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 11 found: MemTotalPlusSwapTotal = MemFree + SwapFree + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 12 found: MemTotalPlusSwapTotal = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 13 found: MemTotalPlusSwapTotal = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 14 found: MemTotalPlusSwapTotal = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 15 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 16 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 17 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 18 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 19 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 20 found: MemTotalPlusSwapTotal = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 21 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonHugePages + Inactive(file) + Active(file) + SReclaimable + Buffers + PageTables + KernelStack
sum 22 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + Buffers + PageTables + KernelStack
sum 23 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + SReclaimable + Buffers + SUnreclaim + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 24 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + Buffers + SUnreclaim + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 25 found: MemTotalPlusSwapTotal = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + Slab + Buffers + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 26 found: MemTotalPlusSwapTotal = MemFree + Inactive + AnonHugePages + Active(file) + DirectMap2M + SReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 27 found: MemTotalPlusSwapTotal = MemFree + Inactive + AnonHugePages + Active(file) + DirectMap2M + KReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 28 found: MemTotalPlusSwapTotal = MemFree + Inactive + AnonHugePages + Active + DirectMap2M + SReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 29 found: MemTotalPlusSwapTotal = MemFree + Inactive + AnonHugePages + Active + DirectMap2M + KReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 30 found: MemTotalPlusSwapTotal = MemFree + Inactive + SwapFree + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 31 found: MemTotalPlusSwapTotal = MemFree + Inactive + SwapFree + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 32 found: MemTotalPlusSwapTotal = MemFree + Inactive + SwapTotal + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 33 found: MemTotalPlusSwapTotal = MemFree + Inactive + SwapTotal + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 34 found: MemTotalPlusSwapTotal = MemTotalMinusSwapTotal + SwapTotal + SwapFree
sum 35 found: MemTotalPlusSwapTotal = MemAvailable + AnonHugePages + Active + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + Buffers + SUnreclaim + DirectMap4k + Mapped + PageTables + Active(anon)
sum 36 found: MemTotalPlusSwapTotal = MemTotalMinusHardwareCorrupted + SwapFree
sum 37 found: MemTotalPlusSwapTotal = MemTotalMinusHardwareCorrupted + SwapTotal
sum 38 found: MemTotalPlusSwapTotal = MemTotalPlusHardwareCorrupted + SwapFree
sum 39 found: MemTotalPlusSwapTotal = MemTotalPlusHardwareCorrupted + SwapTotal
sum 40 found: MemTotalPlusSwapTotal = MemTotal + SwapFree
sum 41 found: MemTotalPlusSwapTotal = MemTotal + SwapTotal
sum 42 found: MemTotalPlusSwapTotal = MemTotalPlusSwapTotalMinusHardwareCorrupted
sum 43 found: MemTotalPlusSwapTotal = MemTotalPlusSwapTotalPlusHardwareCorrupted
sum 44 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 45 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 46 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 47 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = Committed_AS + Inactive + SwapTotal + SwapFree + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 48 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = CommitLimit + Committed_AS + AnonHugePages + Active + DirectMap2M + DirectMap4k + VmallocUsed + Percpu + Mapped + PageTables + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 49 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + SReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 50 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + KReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 51 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active + DirectMap2M + SReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 52 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active + DirectMap2M + KReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 53 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 54 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 55 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapFree + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 56 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 57 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 58 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 59 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapTotal + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 60 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapTotal + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 61 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapTotal + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 62 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 63 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 64 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 65 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Cached + AnonHugePages + Inactive(file) + Active(file) + SReclaimable + Buffers + PageTables + KernelStack
sum 66 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Cached + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + Buffers + PageTables + KernelStack
sum 67 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + SReclaimable + Buffers + SUnreclaim + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 68 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + Buffers + SUnreclaim + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 69 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + Slab + Buffers + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 70 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive + AnonHugePages + Active(file) + DirectMap2M + SReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 71 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive + AnonHugePages + Active(file) + DirectMap2M + KReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 72 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive + AnonHugePages + Active + DirectMap2M + SReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 73 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive + AnonHugePages + Active + DirectMap2M + KReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 74 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive + SwapFree + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 75 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive + SwapFree + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 76 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive + SwapTotal + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 77 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemFree + Inactive + SwapTotal + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 78 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalMinusSwapTotal + SwapTotal + SwapFree
sum 79 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemAvailable + AnonHugePages + Active + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + Buffers + SUnreclaim + DirectMap4k + Mapped + PageTables + Active(anon)
sum 80 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalMinusHardwareCorrupted + SwapFree
sum 81 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalMinusHardwareCorrupted + SwapTotal
sum 82 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalPlusHardwareCorrupted + SwapFree
sum 83 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalPlusHardwareCorrupted + SwapTotal
sum 84 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotal + SwapFree
sum 85 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotal + SwapTotal
sum 86 found: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalPlusSwapTotalMinusHardwareCorrupted
sum 87 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 88 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 89 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Inactive + Cached + SwapTotal + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 90 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = Committed_AS + Inactive + SwapTotal + SwapFree + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 91 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = CommitLimit + Committed_AS + AnonHugePages + Active + DirectMap2M + DirectMap4k + VmallocUsed + Percpu + Mapped + PageTables + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 92 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + SReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 93 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + KReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 94 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active + DirectMap2M + SReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 95 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + Inactive(file) + Active + DirectMap2M + KReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 96 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 97 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 98 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapFree + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 99 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 100 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 101 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 102 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapTotal + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 103 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapTotal + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 104 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapTotal + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 105 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 106 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 107 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + SwapTotal + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 108 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Cached + AnonHugePages + Inactive(file) + Active(file) + SReclaimable + Buffers + PageTables + KernelStack
sum 109 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Cached + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + Buffers + PageTables + KernelStack
sum 110 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + SReclaimable + Buffers + SUnreclaim + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 111 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + Buffers + SUnreclaim + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 112 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Cached + AnonPages + AnonHugePages + DirectMap2M + Slab + Buffers + Percpu + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 113 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive + AnonHugePages + Active(file) + DirectMap2M + SReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 114 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive + AnonHugePages + Active(file) + DirectMap2M + KReclaimable + Buffers + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 115 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive + AnonHugePages + Active + DirectMap2M + SReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 116 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive + AnonHugePages + Active + DirectMap2M + KReclaimable + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize
sum 117 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive + SwapFree + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 118 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive + SwapFree + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 119 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive + SwapTotal + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 120 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemFree + Inactive + SwapTotal + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 121 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalMinusSwapTotal + SwapTotal + SwapFree
sum 122 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemAvailable + AnonHugePages + Active + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + Buffers + SUnreclaim + DirectMap4k + Mapped + PageTables + Active(anon)
sum 123 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalMinusHardwareCorrupted + SwapFree
sum 124 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalMinusHardwareCorrupted + SwapTotal
sum 125 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalPlusHardwareCorrupted + SwapFree
sum 126 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalPlusHardwareCorrupted + SwapTotal
sum 127 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotal + SwapFree
sum 128 found: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotal + SwapTotal
sum 129 found: MemTotal = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 130 found: MemTotal = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 131 found: MemTotal = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 132 found: MemTotal = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 133 found: MemTotal = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 134 found: MemTotal = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 135 found: MemTotal = Committed_AS + Inactive + SwapFree + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 136 found: MemTotal = Committed_AS + Inactive + SwapTotal + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 137 found: MemTotal = MemFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 138 found: MemTotal = MemFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 139 found: MemTotal = MemFree + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 140 found: MemTotal = MemFree + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 141 found: MemTotal = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 142 found: MemTotal = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 143 found: MemTotal = MemFree + Inactive + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 144 found: MemTotal = MemFree + Inactive + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 145 found: MemTotal = MemTotalMinusSwapTotal + SwapFree
sum 146 found: MemTotal = MemTotalMinusSwapTotal + SwapTotal
sum 147 found: MemTotal = MemTotalMinusHardwareCorrupted
sum 148 found: MemTotal = MemTotalPlusHardwareCorrupted
sum 149 found: MemTotalPlusHardwareCorrupted = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 150 found: MemTotalPlusHardwareCorrupted = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 151 found: MemTotalPlusHardwareCorrupted = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 152 found: MemTotalPlusHardwareCorrupted = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 153 found: MemTotalPlusHardwareCorrupted = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 154 found: MemTotalPlusHardwareCorrupted = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 155 found: MemTotalPlusHardwareCorrupted = Committed_AS + Inactive + SwapFree + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 156 found: MemTotalPlusHardwareCorrupted = Committed_AS + Inactive + SwapTotal + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 157 found: MemTotalPlusHardwareCorrupted = MemFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 158 found: MemTotalPlusHardwareCorrupted = MemFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 159 found: MemTotalPlusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 160 found: MemTotalPlusHardwareCorrupted = MemFree + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 161 found: MemTotalPlusHardwareCorrupted = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 162 found: MemTotalPlusHardwareCorrupted = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 163 found: MemTotalPlusHardwareCorrupted = MemFree + Inactive + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 164 found: MemTotalPlusHardwareCorrupted = MemFree + Inactive + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 165 found: MemTotalPlusHardwareCorrupted = MemTotalMinusSwapTotal + SwapFree
sum 166 found: MemTotalPlusHardwareCorrupted = MemTotalMinusSwapTotal + SwapTotal
sum 167 found: MemTotalPlusHardwareCorrupted = MemTotalMinusHardwareCorrupted
sum 168 found: MemTotalMinusHardwareCorrupted = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 169 found: MemTotalMinusHardwareCorrupted = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 170 found: MemTotalMinusHardwareCorrupted = Inactive + Cached + SwapFree + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 171 found: MemTotalMinusHardwareCorrupted = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 172 found: MemTotalMinusHardwareCorrupted = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 173 found: MemTotalMinusHardwareCorrupted = Inactive + Cached + SwapTotal + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 174 found: MemTotalMinusHardwareCorrupted = Committed_AS + Inactive + SwapFree + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 175 found: MemTotalMinusHardwareCorrupted = Committed_AS + Inactive + SwapTotal + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 176 found: MemTotalMinusHardwareCorrupted = MemFree + Inactive(anon) + Inactive(file) + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 177 found: MemTotalMinusHardwareCorrupted = MemFree + Inactive(anon) + Inactive(file) + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 178 found: MemTotalMinusHardwareCorrupted = MemFree + Inactive(anon) + AnonHugePages + DirectMap2M + Slab + KReclaimable + SReclaimable + Mapped + PageTables + Unevictable + Shmem
sum 179 found: MemTotalMinusHardwareCorrupted = MemFree + AnonPages + AnonHugePages + DirectMap2M + KReclaimable + SReclaimable + Buffers + SUnreclaim + VmallocUsed + Shmem
sum 180 found: MemTotalMinusHardwareCorrupted = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + SReclaimable + Buffers + VmallocUsed + Shmem
sum 181 found: MemTotalMinusHardwareCorrupted = MemFree + AnonPages + AnonHugePages + DirectMap2M + Slab + KReclaimable + Buffers + VmallocUsed + Shmem
sum 182 found: MemTotalMinusHardwareCorrupted = MemFree + Inactive + Active(file) + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Active(anon) + Hugepagesize
sum 183 found: MemTotalMinusHardwareCorrupted = MemFree + Inactive + Active + Buffers + SUnreclaim + DirectMap4k + Percpu + Mapped + Hugepagesize
sum 184 found: MemTotalMinusHardwareCorrupted = MemTotalMinusSwapTotal + SwapFree
sum 185 found: MemTotalMinusHardwareCorrupted = MemTotalMinusSwapTotal + SwapTotal
sum 186 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Hugepagesize
sum 187 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Hugepagesize
sum 188 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + Slab + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Hugepagesize
sum 189 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum 190 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active + KReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum 191 found: DirectMap1G = Inactive + Cached + AnonPages + Inactive(anon) + AnonHugePages + Inactive(file) + Active + Slab + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum 192 found: DirectMap1G = Committed_AS + Inactive + Cached + AnonPages + Inactive(anon) + DirectMap2M + SUnreclaim + DirectMap4k + PageTables + Active(anon) + Mlocked + Hugepagesize
sum 193 found: DirectMap1G = MemTotalMinusSwapTotal + Inactive(anon) + KReclaimable + SReclaimable + Buffers + DirectMap4k + VmallocUsed + Percpu + PageTables + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 194 found: DirectMap1G = MemAvailable + Inactive(file) + KReclaimable + SReclaimable + Buffers + SUnreclaim + Mapped + Mlocked + Shmem + KernelStack + Hugepagesize
sum 195 found: DirectMap1G = MemAvailable + Inactive(file) + Slab + SReclaimable + Buffers + Mapped + Mlocked + Shmem + KernelStack + Hugepagesize
sum 196 found: DirectMap1G = MemAvailable + Inactive(file) + Slab + KReclaimable + Buffers + Mapped + Mlocked + Shmem + KernelStack + Hugepagesize
sum 197 found: MemAvailable = Inactive + Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + SReclaimable + SUnreclaim + DirectMap4k + Percpu + PageTables + Unevictable + Shmem
sum 198 found: MemAvailable = Inactive + Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + KReclaimable + SUnreclaim + DirectMap4k + Percpu + PageTables + Unevictable + Shmem
sum 199 found: MemAvailable = Inactive + Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + Slab + DirectMap4k + Percpu + PageTables + Unevictable + Shmem
sum 200 found: MemAvailable = MemTotalMinusSwapTotal + Slab + Buffers + SUnreclaim + DirectMap4k + Active(anon)
sum 201 found: MemTotalMinusSwapTotal = Inactive + Cached + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + SReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 202 found: MemTotalMinusSwapTotal = Inactive + Cached + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + KReclaimable + SUnreclaim + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 203 found: MemTotalMinusSwapTotal = Inactive + Cached + AnonPages + Inactive(anon) + Active + Active(file) + DirectMap2M + Slab + DirectMap4k + Percpu + Mapped + Unevictable + Active(anon)
sum 204 found: MemTotalMinusSwapTotal = Committed_AS + Inactive + Inactive(anon) + Inactive(file) + Active + Slab + Buffers + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Active(anon) + Mlocked + KernelStack + Hugepagesize
sum 205 found: MemFree = SwapTotal + SwapFree + Inactive(anon) + AnonHugePages + Active(file) + DirectMap2M + SUnreclaim + DirectMap4k + VmallocUsed + Shmem + Hugepagesize
sum 206 found: MemFree = Committed_AS + Cached + Inactive(anon) + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Unevictable + Hugepagesize
sum 207 found: CommitLimit = Cached + Inactive(anon) + AnonHugePages + Inactive(file) + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + Buffers + DirectMap4k + VmallocUsed + Percpu + Mapped + Mlocked + Shmem + KernelStack
sum 208 found: CommitLimit = Inactive + Cached + AnonHugePages + Active(file) + DirectMap2M + Slab + KReclaimable + SReclaimable + Buffers + DirectMap4k + VmallocUsed + Percpu + Mapped + Mlocked + Shmem + KernelStack
sum 209 found: CommitLimit = Committed_AS + AnonPages + Inactive(anon) + Active + Active(file) + KReclaimable + SReclaimable + Buffers + VmallocUsed + Percpu + Mapped + Unevictable + Active(anon) + Mlocked + Shmem + KernelStack
sum 210 found: Inactive = Inactive(anon) + Inactive(file)
sum 211 found: Inactive = Cached + Active(file) + Slab + KReclaimable + SReclaimable + VmallocUsed + Percpu + PageTables + Active(anon) + Mlocked + Shmem + KernelStack + Hugepagesize
sum 212 found: Inactive = Cached + Active + Slab + KReclaimable + SReclaimable + VmallocUsed + Percpu + PageTables + Mlocked + Shmem + KernelStack + Hugepagesize
sum 213 found: SwapTotal = SwapFree
sum 214 found: Active = Active(file) + Active(anon)
sum 215 found: Slab = SReclaimable + SUnreclaim
sum 216 found: Slab = KReclaimable + SUnreclaim
sum 217 found: KReclaimable = SReclaimable
exporting sums to alambix98-20240806-223916-sums.json
creating alambix98-20240806-223916-sums.dot from alambix98-20240806-223916-sums.json
creating alambix98-20240806-223916-sums.svg from alambix98-20240806-223916-sums.dot

real    27m11,388s
user    27m11,059s
sys     0m0,144s
last command status : [0]
```


```sh
20240816-12:15:25 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe004$ ../../src/mergesums.py --input-sums-files alambix97-20240805-191606-sums.json alambix98-20240806-223916-sums.json --output-sums-file mergedmeminfosums.json
[PosixPath('alambix97-20240805-191606-sums.json'), PosixPath('alambix98-20240806-223916-sums.json')]
0: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotal + SwapTotal + HardwareCorrupted
1: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalPlusHardwareCorrupted + SwapTotal
2: MemTotalPlusSwapTotalPlusHardwareCorrupted = MemTotalPlusSwapTotal + HardwareCorrupted
3: MemTotalPlusSwapTotal = MemTotalMinusHardwareCorrupted + SwapTotal + HardwareCorrupted
4: MemTotalPlusSwapTotal = MemTotal + SwapTotal
5: MemTotalPlusSwapTotal = MemTotalPlusSwapTotalMinusHardwareCorrupted + HardwareCorrupted
6: MemTotalPlusSwapTotalMinusHardwareCorrupted = MemTotalMinusHardwareCorrupted + SwapTotal
7: MemTotalPlusHardwareCorrupted = MemTotalMinusSwapTotal + SwapTotal + HardwareCorrupted
8: MemTotalPlusHardwareCorrupted = MemTotal + HardwareCorrupted
9: MemTotal = MemTotalMinusSwapTotal + SwapTotal
10: MemTotal = MemTotalMinusHardwareCorrupted + HardwareCorrupted
11: Active = Active(file) + Active(anon)
12: Inactive = Inactive(file) + Inactive(anon)
13: Slab = SReclaimable + SUnreclaim
14: Slab = KReclaimable + SUnreclaim
15: KReclaimable = SReclaimable
16: Inactive = Inactive(anon) + Inactive(file)
last command status : [0]
```

after removing tautologies:
11: Active = Active(file) + Active(anon)
12: Inactive = Inactive(file) + Inactive(anon)
13: Slab = SReclaimable + SUnreclaim
14: Slab = KReclaimable + SUnreclaim
15: KReclaimable = SReclaimable
16: Inactive = Inactive(anon) + Inactive(file)


-> very disappointing: no new interesting equation has been found, most of them are coincidences!

Conclusion: Apart from the ones that we new, no relations seem to exist between most meminfo indicators. As a result, we're still unable to view the meminfo indicators as a a treemap of physical ram.
