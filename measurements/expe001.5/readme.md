find relations between meminfo variables using [/home/graffy/work/linuxram.git/src/expe001.5.py]

```sh
20240814-16:19:42 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe001.5$ ../../src/expe001.5-set001.py 
set of integer variables to explore: [c: 11, b: 8, h: 7, g: 6, d: 5, a: 3, f: 2, e: 1]
sum found: c = d + a + f + e
sum found: c = g + a + f
sum found: c = g + d
sum found: c = h + a + e
sum found: c = b + f + e
sum found: c = b + a
sum found: b = d + f + e
sum found: b = d + a
sum found: b = g + f
sum found: b = h + e
sum found: h = d + f
sum found: h = g + e
sum found: g = a + f + e
sum found: g = d + e
sum found: d = a + f
sum found: a = f + e
exporting sums to set001-sums.json
creating set001-sums.dot from set001-sums.json
creating set001-sums.svg from set001-sums.dot
last command status : [0]
```

```sh
20240814-15:22:42 graffy@graffy-ws2:~/work/linuxram.git/measurements/expe001.5$ ../../src/expe001.5-alambix97.py 
set of integer variables to explore: [MemTotal: 201214205952, DirectMap1G: 199715979264, Committed_AS: 141631254528, CommitLimit: 133373001728, MemAvailable: 131466833920, Cached: 130939863040, Active: 103304773632, Inactive: 94177337344, Inactive(file): 67258384384, AnonPages: 65442127872, Active(file): 63633461248, AnonHugePages: 49218060288, Active(anon): 39671312384, SwapTotal: 32765898752, Inactive(anon): 26918952960, Dirty: 8546779136, DirectMap2M: 6434062336, Slab: 1991671808, KReclaimable: 1252827136, SReclaimable: 1252827136, SwapCached: 1088724992, MemFree: 803061760, SUnreclaim: 738844672, DirectMap4k: 554131456, VmallocUsed: 302333952, PageTables: 218902528, Percpu: 215089152, Mapped: 120565760, Shmem: 69955584, Buffers: 31981568, Unevictable: 23019520, Mlocked: 19873792, KernelStack: 17088512, Writeback: 5550080, Hugepagesize: 2097152, HardwareCorrupted: 53248]
sum found: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum found: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum found: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum found: MemTotal = Active(file) + AnonHugePages + Active(anon) + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum found: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Inactive + Active(file) + Active(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Inactive + Active(file) + Active(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Inactive + Active(file) + Active(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum found: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum found: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Mlocked + Writeback + Hugepagesize + HardwareCorrupted
sum found: MemTotal = Active + AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + KReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Unevictable + Mlocked + KernelStack
sum found: MemTotal = Active + AnonHugePages + Active(anon) + DirectMap2M + SReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum found: MemTotal = Active + AnonHugePages + Active(anon) + DirectMap2M + KReclaimable + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum found: MemTotal = Active + Inactive(file) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Active + Inactive(file) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Active + Inactive(file) + Inactive(anon) + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Active + Inactive + SReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Active + Inactive + KReclaimable + SwapCached + SUnreclaim + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Active + Inactive + Slab + SwapCached + DirectMap4k + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Cached + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SwapCached + MemFree + DirectMap4k + PageTables + Percpu + KernelStack + Writeback + HardwareCorrupted
sum found: MemTotal = Cached + SwapTotal + Inactive(anon) + Dirty + SwapCached + SUnreclaim + Percpu + HardwareCorrupted
sum found: MemTotal = Cached + Active(anon) + Inactive(anon) + SReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum found: MemTotal = Cached + Active(anon) + Inactive(anon) + KReclaimable + SwapCached + SUnreclaim + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum found: MemTotal = Cached + Active(anon) + Inactive(anon) + Slab + SwapCached + PageTables + Percpu + Mapped + Buffers + KernelStack + HardwareCorrupted
sum found: MemTotal = MemAvailable + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SReclaimable + SwapCached + Buffers + Hugepagesize
sum found: MemTotal = CommitLimit + Active(anon) + Inactive(anon) + SUnreclaim + VmallocUsed + Mapped + Shmem + KernelStack + Hugepagesize + HardwareCorrupted
sum found: DirectMap1G = AnonPages + Active(file) + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + SReclaimable + MemFree + DirectMap4k + VmallocUsed + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Hugepagesize + HardwareCorrupted
sum found: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + SReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum found: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + KReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum found: DirectMap1G = Inactive(file) + Active(file) + SwapTotal + Inactive(anon) + DirectMap2M + Slab + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum found: DirectMap1G = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum found: DirectMap1G = Inactive(file) + Active(file) + Active(anon) + Inactive(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum found: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + SReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum found: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + KReclaimable + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum found: DirectMap1G = Inactive + Active(file) + SwapTotal + DirectMap2M + Slab + VmallocUsed + Percpu + Mapped + Shmem + Writeback + HardwareCorrupted
sum found: DirectMap1G = Inactive + Active(file) + Active(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum found: DirectMap1G = Inactive + Active(file) + Active(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum found: DirectMap1G = Active + Active(file) + Inactive(anon) + KReclaimable + SReclaimable + SwapCached + MemFree + SUnreclaim + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum found: DirectMap1G = Active + Active(file) + Inactive(anon) + Slab + SReclaimable + SwapCached + MemFree + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum found: DirectMap1G = Active + Active(file) + Inactive(anon) + Slab + KReclaimable + SwapCached + MemFree + DirectMap4k + Mapped + Unevictable + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum found: DirectMap1G = Active + Inactive(file) + Inactive(anon) + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum found: DirectMap1G = Active + Inactive(file) + Inactive(anon) + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum found: DirectMap1G = Active + Inactive + SReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum found: DirectMap1G = Active + Inactive + KReclaimable + DirectMap4k + VmallocUsed + Shmem + Buffers + KernelStack + Writeback
sum found: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
sum found: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + Slab + SReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
sum found: DirectMap1G = Cached + AnonHugePages + Dirty + DirectMap2M + Slab + KReclaimable + DirectMap4k + VmallocUsed + PageTables + Percpu + Unevictable + KernelStack + Hugepagesize + HardwareCorrupted
sum found: DirectMap1G = Cached + Active(file) + KReclaimable + SReclaimable + MemFree + SUnreclaim + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
sum found: DirectMap1G = Cached + Active(file) + Slab + SReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
sum found: DirectMap1G = Cached + Active(file) + Slab + KReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Shmem + Mlocked + KernelStack + HardwareCorrupted
sum found: DirectMap1G = Cached + Inactive(file) + SwapCached + VmallocUsed + Shmem + Buffers + KernelStack + Writeback + Hugepagesize
sum found: DirectMap1G = MemAvailable + AnonPages + SReclaimable + SwapCached + VmallocUsed + Mapped + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: DirectMap1G = MemAvailable + AnonPages + KReclaimable + SwapCached + VmallocUsed + Mapped + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: Committed_AS = AnonHugePages + Active(anon) + SwapTotal + Dirty + DirectMap2M + Slab + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mapped + Unevictable + Mlocked + Writeback + Hugepagesize
sum found: Committed_AS = Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: Committed_AS = Active(file) + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + KReclaimable + MemFree + DirectMap4k + PageTables + Percpu + Mapped + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: Committed_AS = AnonPages + SwapTotal + Inactive(anon) + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mapped + Shmem + Buffers + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum found: Committed_AS = Inactive(file) + SwapTotal + Inactive(anon) + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Unevictable + Mlocked + KernelStack + Hugepagesize
sum found: Committed_AS = Inactive + Inactive(anon) + Dirty + DirectMap2M + Slab + SwapCached + MemFree + SUnreclaim + VmallocUsed + PageTables + Percpu + Mapped + Buffers + Unevictable + Mlocked + HardwareCorrupted
sum found: Committed_AS = Inactive + SwapTotal + Dirty + Slab + KReclaimable + SReclaimable + SUnreclaim + DirectMap4k + PageTables + Shmem + Unevictable + Mlocked + KernelStack + Hugepagesize
sum found: Committed_AS = Cached + DirectMap2M + Slab + MemFree + SUnreclaim + DirectMap4k + Shmem + Buffers + Unevictable + Mlocked + KernelStack + Writeback + Hugepagesize + HardwareCorrupted
sum found: Committed_AS = CommitLimit + DirectMap2M + SwapCached + VmallocUsed + Percpu + Mapped + Shmem + Mlocked + Writeback + Hugepagesize
sum found: CommitLimit = AnonHugePages + Active(anon) + SwapTotal + Dirty + SwapCached + MemFree + DirectMap4k + VmallocUsed + Percpu + Mapped + Shmem + KernelStack
sum found: CommitLimit = Active(file) + AnonHugePages + Dirty + DirectMap2M + Slab + SwapCached + MemFree + SUnreclaim + DirectMap4k + PageTables + Mapped + KernelStack + Writeback + Hugepagesize
sum found: CommitLimit = Inactive(file) + SwapTotal + Inactive(anon) + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
sum found: CommitLimit = Inactive + SwapTotal + Slab + KReclaimable + SReclaimable + SwapCached + DirectMap4k + Percpu + Buffers + Mlocked + KernelStack + Writeback
sum found: CommitLimit = Cached + SwapCached + MemFree + VmallocUsed + Mapped + Shmem + Unevictable + Mlocked + Writeback + HardwareCorrupted
sum found: MemAvailable = AnonPages + AnonHugePages + Dirty + DirectMap2M + MemFree + DirectMap4k + VmallocUsed + Mapped + Unevictable + KernelStack + Writeback + HardwareCorrupted
sum found: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum found: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum found: MemAvailable = Inactive(file) + SwapTotal + Inactive(anon) + Slab + SwapCached + MemFree + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum found: MemAvailable = Inactive(file) + Active(file) + PageTables + Percpu + Shmem + Buffers + Mlocked + KernelStack + Hugepagesize
sum found: MemAvailable = Inactive + SwapTotal + SReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum found: MemAvailable = Inactive + SwapTotal + KReclaimable + SwapCached + MemFree + SUnreclaim + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum found: MemAvailable = Inactive + SwapTotal + Slab + SwapCached + MemFree + VmallocUsed + Percpu + Mapped + Hugepagesize + HardwareCorrupted
sum found: Cached = AnonPages + SwapTotal + Inactive(anon) + Slab + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + Percpu + Shmem + Buffers + KernelStack
sum found: Cached = AnonPages + AnonHugePages + Dirty + DirectMap2M + SUnreclaim + VmallocUsed + Percpu + Mlocked + KernelStack + Writeback + HardwareCorrupted
sum found: Active = Active(anon) + SwapTotal + Inactive(anon) + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mlocked + Writeback + Hugepagesize
sum found: Active = Active(file) + Active(anon)
sum found: Inactive = AnonHugePages + Inactive(anon) + Dirty + DirectMap2M + Slab + SUnreclaim + PageTables + Shmem + Unevictable + KernelStack
sum found: Inactive = Inactive(file) + Inactive(anon)
sum found: Inactive(file) = AnonHugePages + Dirty + DirectMap2M + Slab + SUnreclaim + PageTables + Shmem + Unevictable + KernelStack
sum found: Active(file) = SwapTotal + Inactive(anon) + SwapCached + MemFree + SUnreclaim + DirectMap4k + VmallocUsed + PageTables + Percpu + Mlocked + Writeback + Hugepagesize
sum found: Slab = SReclaimable + SUnreclaim
sum found: Slab = KReclaimable + SUnreclaim
sum found: KReclaimable = SReclaimable
exporting sums to alambix97-meminfo-sums.json
creating alambix97-meminfo-sums.dot from alambix97-meminfo-sums.json
creating alambix97-meminfo-sums.svg from alambix97-meminfo-sums.dot
last command status : [0]
```

note: stdout stored in [text](simplification/treemap000.md) after manual cleanup
